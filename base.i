/* -*- mode: c++ -*- */
/*
   Copyright 2009 Advanced Micro Devices, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

#ifdef SWIGPERL
%module "oa::base";
#endif

#ifdef SWIGRUBY
%module "oa::base";
#endif

#ifdef SWIGTCL
%module "base";
#endif

#ifdef SWIGPYTHON
%module "base";
#endif

#ifdef SWIGCSHARP
%module "oasCSharpBase";
#endif

%{

#include <oa/oaDesignDB.h>
#include <oa/oaWaferDB.h>

USE_OA_NAMESPACE
USE_OA_COMMON_NAMESPACE
USE_OA_PLUGIN_NAMESPACE

#undef SWIG_SHADOW
#define SWIG_SHADOW 0

// Call oaBaseInit automatically to avoid SEGV if the user forgets.
// The user can choose to NOT call this in the event that an older OA DM
// model is desired by setting $OASCRIPT_USE_INIT = 0.  Scripting languages
// execute this via SWIG_init(), C# via "%pragma(csharp) imclasscode".
static void oasBaseInit()
{
	char *useinit = getenv("OASCRIPT_USE_INIT");
	if (!useinit || strcmp(useinit, "0")) {
		OpenAccess_4::oaBaseInit();
	}
}
%}

%include <default_namespace.i>

// Basic agnostic setup
%include "oa.i"

// Optional target-language-specific code:
%include <target_base_pre.i>

#if !defined(oaBase_P)
#define oaBase_P

// Use the native language interface for interacting with files and directories
// instead of the OA version.
%ignore oaFile;
%ignore oaDir;
%ignore oaDirIter;
%ignore oaFSComponent;

// *****************************************************************************
// Namespace and basic type declarations and definitions
// *********************************1********************************************
%import "common.i"

%include "oa/oaBaseTypes.h"

// *****************************************************************************
// Public Header Files
// *****************************************************************************
%include "oa/oaBaseModTypes.h"
%include "oa/oaBaseMsgs.h"
%include "oa/oaMemory.h"
%include "oa/oaArray.h"

%include <target_arrays.i>

%include "oa/oaString.h"
%include "oa/oaOrient.h"
%include "oa/oaPoint.h"
%include "oa/oaVector.h"
%include "oa/oaSegment.h"
%include "oa/oaBox.h"
%include "oa/oaPointArray.h"
%include "oa/oaTransform.h"
%include "oa/oaTimeStamp.h"
%include "oa/oaComplex.h"
%include "oa/oaRange.h"
%include "oa/oaPackedData.h"
%include "oa/oaFile.h"
%include "oa/oaMapFile.h"
%include "oa/oaMapWindow.h"
%include "oa/oaException.h"
%include "oa/oaTimer.h"
%include "oa/oaType.h"
%include "oa/oaDomain.h"
%include "oa/oaObject.h"
%include "oa/oaObjectArray.h"
%include "oa/oaProp.h"
%include "oa/oaIntProp.h"
%include "oa/oaFloatProp.h"
%include "oa/oaStringProp.h"
%include "oa/oaAppProp.h"
%include "oa/oaHierProp.h"
%include "oa/oaDoubleProp.h"
%include "oa/oaBooleanProp.h"
%include "oa/oaTimeProp.h"
%include "oa/oaIntRangeProp.h"
%include "oa/oaFloatRangeProp.h"
%include "oa/oaDoubleRangeProp.h"
%include "oa/oaTimeRangeProp.h"
%include "oa/oaEnumProp.h"
%include "oa/oaGroup.h"
%include "oa/oaNameSpace.h"
%include "oa/oaName.h"
%include "oa/oaSessionObject.h"
%include "oa/oaSession.h"
%include "oa/oaAppObjectDef.h"
%include "oa/oaAppDef.h"
%include "oa/oaFeature.h"
%include "oa/oaCdbaNS.h"
%include "oa/oaNativeNS.h"
%include "oa/oaLefDefNS.h"
%include "oa/oaSpiceNS.h"
%include "oa/oaSpefNS.h"
%include "oa/oaSpfNS.h"
%include "oa/oaUnixNS.h"
%include "oa/oaVerilogNS.h"
%include "oa/oaVhdlNS.h"
%include "oa/oaWinNS.h"
%include "oa/oaCollection.h"
%include "oa/oaBaseCollection.h"
%include "oa/oaHashTbl.h"
%include "oa/oaHashSet.h"
%include "oa/oaHashMap.h"
%include "oa/oaLookupTbl.h"

%include <target_lookuptables.i>

%include "oa/oaParam.h"
%include "oa/oaValue.h"
%include "oa/oaConstraintGroupDef.h"
%include "oa/oaConstraintParamDef.h"
%include "oa/oaConstraintParam.h"
%include "oa/oaConstraintDef.h"
%include "oa/oaConstraint.h"
%include "oa/oaGroupDef.h"
%include "oa/oaSimpleConstraint.h"
%include "oa/oaConstraintGroup.h"
%include "oa/oaConstraintGroupHeader.h"
%include "oa/oaXYTree.h"
%include "oa/oa2DSpace.h"
%include "oa/oaBaseTraits.h"
%include "oa/oaObserver.h"
%include "oa/oaBaseObserver.h"
%include "oa/oaSocket.h"
%include "oa/oaMutex.h"
%include "oa/oaBuildInfo.h"
%include "oa/oaBuildDef.h"



// *****************************************************************************
// Public Inline Function Files
// *****************************************************************************
%include "oa/oaArray.inl"
%include "oa/oaString.inl"
%include "oa/oaOrient.inl"
%include "oa/oaPoint.inl"
%include "oa/oaVector.inl"
%include "oa/oaSegment.inl"
%include "oa/oaBox.inl"
%include "oa/oaPointArray.inl"
%include "oa/oaTransform.inl"
%include "oa/oaTimeStamp.inl"
%include "oa/oaComplex.inl"
%include "oa/oaRange.inl"
%include "oa/oaPackedData.inl"
%include "oa/oaFile.inl"
%include "oa/oaMapFile.inl"
%include "oa/oaMapWindow.inl"
%include "oa/oaException.inl"
%include "oa/oaTimer.inl"
%include "oa/oaType.inl"
%include "oa/oaDomain.inl"
%include "oa/oaObject.inl"
%include "oa/oaObjectArray.inl"
%include "oa/oaGroup.inl"
%include "oa/oaNameSpace.inl"
%include "oa/oaName.inl"
%include "oa/oaAppDef.inl"
%include "oa/oaFeature.inl"
%include "oa/oaCdbaNS.inl"
%include "oa/oaNativeNS.inl"
%include "oa/oaLefDefNS.inl"
%include "oa/oaSpiceNS.inl"
%include "oa/oaSpefNS.inl"
%include "oa/oaSpfNS.inl"
%include "oa/oaUnixNS.inl"
%include "oa/oaVerilogNS.inl"
%include "oa/oaVhdlNS.inl"
%include "oa/oaWinNS.inl"
%include "oa/oaCollection.inl"
%include "oa/oaHashTbl.inl"
%include "oa/oaHashSet.inl"
%include "oa/oaHashMap.inl"
%include "oa/oaLookupTbl.inl"
%include "oa/oaParam.inl"
%include "oa/oaValue.inl"
%include "oa/oaConstraintGroupDef.inl"
%include "oa/oaConstraintParamDef.inl"
%include "oa/oaConstraintParam.inl"
%include "oa/oaConstraintDef.inl"
%include "oa/oaGroupDef.inl"
%include "oa/oaXYTree.inl"
%include "oa/oa2DSpace.inl"
%include "oa/oaObserver.inl"
%include "oa/oaBaseObserver.inl"
%include "oa/oaSocket.inl"
%include "oa/oaMutex.inl"
%include "oa/oaBuildInfo.inl"


#endif

// Optional target-language-specific code:
%include <target_base_post.i>

%include <generic/extensions/base.i>

#if !defined(SWIGCSHARP)
%init %{
  // Call oaBaseInit automatically.
  oasBaseInit();
%}
#endif

