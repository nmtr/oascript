
# load oaTcl into the interpreter
set auto_path [concat $env(OA_ROOT)/lib/linux_rhel40_64/opt $auto_path]
puts "Loading oaTcl..."
package require oa

# move oaTcl to a new namespace
puts "Moving oaTcl to 'oaTcl::' namespace"
array set commands {}
foreach command [info commands oa::*] {
    set l [string map {:: " "} $command]
    set ns [lindex $l 0]
    set cmd [lindex $l 1]
    set newCmd "oaTcl::$cmd"
    rename $command $newCmd
    set commands($newCmd) $command
}

array set vars {}
foreach var [info vars oa::*] {
    set l [string map {:: " "} $var]
    set ns [lindex $l 0]
    set name [lindex $l 1]
    set newVar "oaTcl::$name"
    set $newVar $$var
    unset $var
    set vars($newVar) $var
}

# now load oasTcl into the interpreter
puts "Loading oasTcl..."
set auto_path [file dirname [info script]]
package forget oa
package require oa

array set specialCommands {
    "pushNameSpace" "oaNameSpace_push"
    "popNameSpace" "oaNameSpace_pop"
    "getNameSpace" "oaNameSpace_getDefault"
}

proc determineCommandType { command } {
    # Member functions start with a lower case letter
    # Special "Enum" functions end with "Enum"
    # Constructors and static member functions start with an upper case letter
    # Constructors return a class of the form "oa<class>" or "<class>"
    # Static member functions can have any other return value
    if {[info exists ::specialCommands($command)]} {
        return "specialCommand"
    }
    if {[string is lower [string range $command 0 0]]} {
        return "memberFunction"
    }
    if {[regexp ".*Enum" $command]} {
        return "enumFunction"
    }
    if {[regexp ".*Init" $command]} {
        return "initFunction"
    }
    set help [oaTcl::help $command]
    foreach line [split $help "\n"] {
        # Most constructors create oa::oa<class> objects
        if {[regexp "==> oa(.*)" $line wholeMatch class] && $class == $command} {
            return "constructor"
        }
        # A small number create oa::<class> objects
        if {[regexp "==> (.*)" $line wholeMatch class] && $class == $command} {
            return "constructor"
        }
    }
    return "staticFunction"
}

proc commandReturnType { command } {
    set help [oaTcl::help $command]
    foreach line [split $help "\n"] {
        if {[regexp "==> oaCollection_.*" $line]} {
            return "oaCollection"
        } elseif {[regexp "==> oaBox.*" $line]} {
            return "oaBox"
        } elseif {[regexp "==> oaPoint.*" $line]} {
            return "oaPoint"
        }
    }
    return "generic"
}

proc processCommands {fileHandle} {
    foreach command [lsort [info commands "oaTcl::*"]] {
        set l [string map {:: " "} $command]
        set cmd [lindex $l 1]
        set commandType [determineCommandType $cmd]
        set returnType [commandReturnType $cmd]
        switch $commandType {
            "specialCommand" {
                wrapSpecial $fileHandle $cmd
            }
            "constructor" {
                wrapConstructor $fileHandle $cmd $returnType
            }
            "staticFunction" {
                wrapStaticFunction $fileHandle $cmd $returnType
            }
            "memberFunction" {
                wrapMemberFunction $fileHandle $cmd $returnType
            }
            "enumFunction" {
                wrapEnumFunction $fileHandle $cmd
            }
            "initFunction" {
                wrapInitFunction $fileHandle $cmd
            }
        }
    }
}

proc writeReturnClause { fileHandle { returnType "" } } {
    puts $fileHandle "        if {\$result == \"NULL\"} {"
    puts $fileHandle "            return {}"
    puts $fileHandle "        } else {"
    switch $returnType {
        "oaCollection" {
            puts $fileHandle "            return \[\$result createIter\]"
        }
        "oaBox" {
            puts $fileHandle "            return \[\$result toCoords\]"
        }
        "oaPoint" {
            puts $fileHandle "            return \[\$result toStr\]"
        }
        default {
            puts $fileHandle "            return \$result"
        }
    }
    puts $fileHandle "        }"
}

proc wrapSpecial { fileHandle command } {
    set cmd $::specialCommands($command)
    if {$cmd == ""} {
        puts "Warning: skipped command '$command'"
        return
    }
    puts $fileHandle ""
    puts $fileHandle "    # Special command '$command'"
    puts $fileHandle "    proc $command { args } {"
    puts $fileHandle "        ::set result \[eval $cmd \$args\]"
    writeReturnClause $fileHandle
    puts $fileHandle "    }"
}

proc wrapConstructor { fileHandle command returnType } {
    if {[info commands oa::$command] != {}} {
        puts "$command identical"
        return
    } elseif {[info commands oa::oa$command] != {}} {
        set cmd "oa${command}"
    } else {
        wrapUnavailable $fileHandle $command
        return
    }
    puts $fileHandle ""
    puts $fileHandle "    # Constructor for '$cmd'"
    puts $fileHandle "    proc $command { args } {"
    puts $fileHandle "        ::set result \[eval $cmd \$args\]"
    writeReturnClause $fileHandle $returnType
    puts $fileHandle "    }"
}

proc determineOasTclName { command } {
    set result ""
    set len [string length $command]
    for {set i $len} {$i >=0} {incr i -1} {
        set char [string range $command $i $i]
        if {[string is upper $char]} {
            set head [string range $command 0 [expr $i - 1]]
            set mid [string tolower $char]
            set tail [string range $command [expr $i + 1] end]
            set checkCmd "oa${head}_${mid}${tail}"
            if {[info commands oa::$checkCmd] != {}} {
                if {$result != ""} {
                    puts "Warning: found multiple matches for $command"
                }
                set result $checkCmd
            }
        }
    }
    if {$result == ""} {
        set checkCmd "oa${command}"
        if {[info commands oa::$checkCmd] != {}} {
            set result $checkCmd
        }
    }
    return $result
}

proc wrapUnavailable { fileHandle command } {
    puts "Warning: could not wrap '$command'"
    puts $fileHandle ""
    puts $fileHandle "    # Unsupported function"
    puts $fileHandle "    proc $command { args } {"
    puts $fileHandle "        error \"This call is currently unsupported in the oaTcl compatibilty API.\""
    puts $fileHandle "    }"
}

proc wrapStaticFunction { fileHandle command returnType } {
    set cmd [determineOasTclName $command]
    if {$cmd == ""} {
        wrapUnavailable $fileHandle $command
        return
    }
    puts $fileHandle ""
    puts $fileHandle "    # Static member function"
    puts $fileHandle "    proc $command { args } {"
    puts $fileHandle "        ::set result \[eval ${cmd} \$args\]"
    writeReturnClause $fileHandle $returnType
    puts $fileHandle "    }"
}

proc wrapMemberFunction { fileHandle command returnType } {
    if {[info commands "oa::*_$command"] == {}} {
        wrapUnavailable $fileHandle $command
        return
    }
    puts $fileHandle ""
    puts $fileHandle "    # Class member function '$command'"
    puts $fileHandle "    proc $command { object args } {"
    puts $fileHandle "        ::set result \[eval \$object $command \$args\]"
    writeReturnClause $fileHandle $returnType
    puts $fileHandle "    }"
}

proc wrapEnumFunction { fileHandle command } {
    regexp "(.*)Enum" $command wholeMatch objectName
    puts $fileHandle ""
    puts $fileHandle "    # Enum function for $objectName"
    puts $fileHandle "    proc $command { object args } {"
    puts $fileHandle "        ::set result \[eval \$object typeEnum \$args\]"
    writeReturnClause $fileHandle
    puts $fileHandle "    }"
}

proc wrapInitFunction { fileHandle command } {
    regexp "(.*)Init" $command wholeMatch moduleName
    puts $fileHandle ""
    puts $fileHandle "    # Init function for module oa$moduleName"
    puts $fileHandle "    proc $command { args } {"
    puts $fileHandle "        ::set result \[eval oa${moduleName}Init \$args\]"
    writeReturnClause $fileHandle
    puts $fileHandle "    }"
}

proc writeHeader { fileHandle } {
    puts $fileHandle "# This is file auto-generated."
    puts $fileHandle ""
    puts $fileHandle "package provide oaTclCompat 0.0"
    puts $fileHandle ""
    puts $fileHandle "namespace eval oa \{"
}

proc writeFooter { fileHandle } {
    puts $fileHandle "\}"
}

set fh [open "oaTclCompat.tcl" "w"]
writeHeader $fh

# process commands
puts "Processing commands..."
processCommands $fh

writeFooter $fh

puts "Done!"
close $fh

