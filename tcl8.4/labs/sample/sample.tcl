#!/usr/bin/env tcl
################################################################################
#
#  Copyright (c) 2002-2010  Si2, Inc.  DUNS 62-191-1718
#
#  Licensed under the Apache License, Version 2.0 (the "License"] you may not use
#  this file except in compliance with the License. You may obtain a copy of the
#  License at http:#www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software distributed
#  under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
#  CONDITIONS OF ANY KIND, either express or implied. See the License for the
#  specific language governing permissions and limitations under the License.
#
################################################################################
#   Version History
#
#   DATE      VERSION  BY               DESCRIPTION
#   08/09/10  10.0     Si2              Tutorial 10th Edition - Tcl version
#
################################################################################
#
#   Acknowledgments
#
#   The code herein uses syntax, concepts, appearance, style, formats, and
#   techniques, that appear in several copyrighted works without explicit
#   permisssion or attribution but in good faith within the limits of fair and
#   legal use to the extent known by the author[s]. Such works include,
#   - Classroom and Computer-Aided Instruction (CAI) projects from various
#     industries, including related pedagogical techniques, exercise and lab
#     formats, lesson-plans, and instructional models.
#   - K&R C
#   - Stroustroup C++
#   - ANSI C++
#   - C Library (UNIX man pages)
#   - The OpenAccess API and Reference Implementation and related materials
#   - The SWIG development tool and related documentation (www.swig.org)
#
#   The following members of the Scripting Languages Working Group, created the
#   initial version of the infrastructure and code to produce these lab code
#   examples based on the original Si2 OpenAccess Tutorial labs:
#
#     Rudy Albachten, Chair
#     James Masters
#     Christian Delbaere
#     Steve Potter
#     Stefan Zager
#     Carl Olson
#     Doug Keller
#     Koushik Kalyanaraman
#     Brandon Barclay
#
################################################################################

#  Each PathSeg represented by a - line representing a different Route
#  with the function creating them listed underneath.
#  _________________________________________________________________________
#  |Top                                                                      |
#  |    ___________________    ___________________    ___________________    |
#  |   |Gate               |  |Gate               |  |Gate               |   |
#  |   |             -     |  |             -     |  |             -     |   |
#  |   |             -     |  |             -     |  |             -     |   |
#  |   |  -          -     |  |  -          -     |  |  -          -     |   |
#  |   |  -     NetShapeRoute |  -     NetShapeRoute |  -     NetShapeRoute  |
#  |   |  --               |  |  --               |  |  --               |   |
#  |   |  -                |  |  -                |  |  -                |   |
#  |   |  -                |  |  -                |  |  -                |   |
#  |   |  --    OrInst     |  |  --    OrInst     |  |  --    OrInst     |   |
#  |   |  ---   NandInst1  |  |  ---   NandInst1  |  |  ---   NandInst1  |   |
#  |   |  ---   NandInst2  |  |  ---   NandInst2  |  |  ---   NandInst2  |   |
#  |   |  -     InvInst    |  |  -     InvInst    |  |  -     InvInst    |   |
#  |   |  -                |  |  -                |  |  -                |   |
#  |   |  -                |  |  -                |  |  -                |   |
#  |   | ConnectRouteForNet|  | ConnectRouteForNet|  | ConnectRouteForNet|   |
#  |   |___________________|  |___________________|  |___________________|   |
#  |                                                                         |
#  |        -                                                                |
#  |        -                                                                |
#  |        -                   -                                            |
#  |        -                RouteForNet                                     |
#  |        -                                                                |
#  |    EmptyNets                                                            |
#  |_________________________________________________________________________|


package require oa


# ****************************** Gratuitous globals ******************************

variable mode_read  "r"
variable mode_write "w"

proc assert {condition msg} {
    if {!$condition} {
        puts "***ASSERT error $msg"
    }
}

# ****************************** Functions ******************************

proc getLayerPurpose {design layerName purpName num purp} {
    upvar 1 $num  layerNum
    upvar 1 $purp layerPurp

    set techFile [$design getTech]

    if {$techFile == "NULL"} {
        if {$layerName == "pin"} {
            set layerNum 229;
        } elseif {$layerName == "device"} {
            set layerNum 231
        }
        set layerPurp 252
        return 1
    }

    set layer [oa::oaLayer_find $techFile $layerName]
    set purpose [oa::oaPurpose_find $techFile $purpName]

    if { {$layer == "NULL"} || {$purpose == "NULL"} } {
        return 0
    }

    set layerNum [$layer getNumber]
    set layerPurp [$purpose getNumber]

    return 1
}

# *****************************************************************************
# Create Route with the ObjectArray, attach to the specified net, set end Conns
# *****************************************************************************

proc connectRouteForNet {block net routeObjectArray start end} {
    set route [oa::oaRoute_create $block]
    $route setObjects $routeObjectArray
    $route addToNet $net
    if {$start != "NULL" } {
        $route setBeginConn $start
    }
    if {$end != "NULL"} {
        $route setEndConn $end
    }
}

# *****************************************************************************
# Create oaInstTerm object with the given parameters, which may not be bound.
# *****************************************************************************

proc createInstTerm {net inst termName} {
    set ns [oa::oaNativeNS]

    # It seems that we can't use any subclass of oaName for creating InstTerms.
    #set simpleTermName [oa::oaSimpleName $ns $termName]
    #return [oa::oaInstTerm_create $net $inst [oa::oaSimpleName $ns $termName]]

    #set scTermName [oa::oaScalarName $ns $termName]
    #return [oa::oaInstTerm_create $net $inst $scTermName]

    set name  [oa::oaName $ns $termName]
    return [oa::oaInstTerm_create $net $inst $name]
}

# *****************************************************************************
# Create an InstTerm object for the given terminal with 'termName' in 'inst'
# *****************************************************************************

proc getInstTerm {inst net termName} {
    set instTerm [createInstTerm $net $inst $termName]
    if {[$instTerm getTerm] == "NULL"} {
        puts "Inst term for $termName unbound. Unable to create InstTerm"
        return NULL;
    } else {
        return $instTerm;
    }
}

# *****************************************************************************
# Create a Pin with the given name, Points and direction, on 'term'
# *****************************************************************************

proc createPin {block term points name dir} {
    set layer ""
    set purpose ""
    getLayerPurpose [$block getDesign] "pin" "drawing" layer purpose
    set fig [oa::oaPolygon_create $block $layer $purpose  $points]
    set pin [oa::oaPin_create $term $name $dir]
    $fig addToPin $pin
    return $pin
}


# *****************************************************************************
# Create a Net with the given name in the given Block.
# *****************************************************************************

proc createNet {block str} {
    set ns [oa::oaNativeNS]
    set name [oa::oaScalarName $ns $str]
    return [oa::oaScalarNet_create $block $name]
}


# *****************************************************************************
# Create a Term with the given name on the given Net.
# *****************************************************************************

proc createTerm {net str type} {
    set ns [oa::oaNativeNS]
    set name [oa::oaScalarName $ns $str]
    return [oa::oaScalarTerm_create $net $name $type]
}


# ***************************************************************************
# createNetShapeRoute()
# ***************************************************************************

proc createNetShapeRoute {design block refx refy} {
    # Create path shape, route, terminal and net
    set net [createNet $block "ShapeRouteNet"]
    createTerm $net "TermOnShapeRouteNet" $oa::oacInputTermType

    set lNum ""
    set pNum ""

    set beginExt 1
    set endExt   1
    set width    2
    getLayerPurpose $design "device" "drawing1" lNum pNum

    set endStyle [oa::oaEndStyle "variable"]
    set segSty [oa::oaSegStyle $width $endStyle $endStyle $beginExt $endExt]

    # Route  is created.
    set point1 [list $refx $refy]
    set point2 [list [expr $refx+100] $refy]
    set seg1 [oa::oaPathSeg_create $block $lNum $pNum $point1 $point2 $segSty]

    set routeObjectArray1 [list $seg1]

    set route1 [oa::oaRoute_create $block]
    $route1 setObjects $routeObjectArray1

    puts "Unconnected route is created in Gate view"

    # Creating the shape: Ellipse

    set centerx [expr $refx+100]
    set centery $refy

    set ellRect [list [expr $centerx-10] [expr $centery-10]   \
                      [expr $centerx+10] [expr $centery+10]]

    set fig [oa::oaEllipse_create $block $lNum $pNum $ellRect]
    $fig addToNet $net

    puts "An Ellipse is created in the Gate view"

    $route1 addToNet $net
    $route1 setEndConn $fig

    set startx $centerx
    set starty $centery

    set point1 [oa::oaPoint $startx $starty]
    set point2 [oa::oaPoint [expr $startx+50] $starty]
    set seg2 [oa::oaPathSeg_create $block $lNum $pNum $point1 $point2 $segSty]

    set routeObjectArray2 [list $seg2]

    set route2 [oa::oaRoute_create $block]
    $route2 setObjects $routeObjectArray2
    $route2 addToNet $net
    $route2 setBeginConn $fig

    set p1 [list [expr $startx+55] $starty]
    set p2 [list [expr $startx+65] $starty]
    set pArray [list $p1 $p2]

    set netPath [oa::oaPath_create $block $lNum $pNum 1 $pArray]
    $netPath addToNet $net

    puts "Net with Shape Route is created in Gate view"

    set point1 [list [expr $startx+70] [expr $starty+5]]
    set point2 [list [expr $startx+70] [expr $starty+20]]

    set seg3 [oa::oaPathSeg_create $block $lNum $pNum $point1 $point2 $segSty]

    set routeObjectArray3 [list $seg3]

    set route3 [oa::oaRoute_create $block]
    $route3 setObjects $routeObjectArray3

    set p1 [list [expr $startx+70] [expr $starty-5]]
    set p2 [list [expr $startx+70] [expr $starty-20]]
    set pArray2 [list $p1 $p2]

    oa::oaPath_create $block $lNum $pNum 1 $pArray2
}


# *****************************************************************************
# Create a ScalarInst in a Block with the given cellname. The View is
# assumed to be 'schematic for this purpose. Open the Design if it is not
# already open in the current session.
# *****************************************************************************

proc createInstance {block trans cellName instStr scNameLib} {
    variable mode_read

    set ns [oa::oaNativeNS]
    set gate [oa::oaScalarName $ns $cellName]
    set view [oa::oaScalarName $ns "schematic"]
    set master [oa::oaDesign_find $scNameLib $gate $view]
    if {$master == "NULL"} {
        set master [oa::oaDesign_open $scNameLib $gate $view $mode_read]
    }
    if {$master == "NULL"} {
        return "NULL"
    }
    set instName [oa::oaScalarName $ns $instStr]
    return [oa::oaScalarInst_create $block $master $instName $trans]
}


# *****************************************************************************
# Create a gate schematic which contains 4 gates, (Or, 2 Nand, and an Inv) and
# connect the gates with physical and logical elements. Routes are used for
# physical connections. The created Design is represented by 'Lib' 'Gate'
# 'schematic' (lib, cell and view names), respectively
# *****************************************************************************

proc createGateSchematic {scNameLib} {
    variable mode_write

    set ns [oa::oaNativeNS]
    set cell [oa::oaScalarName $ns "Gate"]
    set view [oa::oaScalarName $ns "schematic"]

    puts "Creating Gate schematic with 0r, 2 nand, Inv instances"

    set vtSch [oa::oaViewType_get [oa::oaReservedViewType "schematic"]]
  
    set design [oa::oaDesign_open $scNameLib $cell $view $vtSch $mode_write]
    $design setCellType [oa::oaCellType "softMacro"]
    set block [oa::oaBlock_create $design]

    set trans [oa::oaTransform 0 0]
    set orInst [createInstance $block $trans "Or" "OrInst" $scNameLib]

    set orBox [$orInst getBBox]

    set height [$orBox getHeight]
    set width  [$orBox getWidth]

    set xMid [$orBox left]
    set yMid [expr [$orBox bottom]+$height / 2]

    set trans [oa::oaTransform 0 [expr 0-2 * $height]]
    set nand1 [createInstance $block $trans "Nand" "NandInst1" $scNameLib]

    set nand1Box [$nand1 getBBox]
    set yMid2 [expr [$nand1Box bottom]+$height / 2]

    set net1 [createNet $block "N1"]
    set bbox [list 0 0 0 0]
    set stein1 [oa::oaSteiner_create $block $bbox]

    set lNum ""
    set pNum ""

    set beginExt 0
    set endExt   0
    set widthRt  2

    getLayerPurpose $design "device" "drawing1" lNum pNum

    set endStyle [oa::oaEndStyle "truncate"]
#    set segSty [oa::oaSegStyle $widthRt $oa::oacTruncateEndStyle $oa::oacTruncateEndStyle $beginExt $endExt]
    set segSty [oa::oaSegStyle $widthRt $endStyle $endStyle $beginExt $endExt]

    set point1 [list [expr $xMid-50] [expr $yMid+10]]
    set point2 [list [expr $xMid-15] [expr $yMid+10]]
    set seg1 [oa::oaPathSeg_create $block $lNum $pNum $point1 $point2 $segSty]

    set routeObjectArray1 [list $seg1]

    set ptArray [list \
                [list [expr $xMid-70] [expr $yMid+15]] \
                [list [expr $xMid-55] [expr $yMid+15]] \
                [list [expr $xMid-50] [expr $yMid+10]] \
                [list [expr $xMid-55] [expr $yMid+5]] \
                [list [expr $xMid-70] [expr $yMid+5]] ]

    set term1 [createTerm $net1 "a" $oa::oacInputTermType]

    set pin1 [createPin $block $term1 $ptArray "P1-In" $oa::oacLeft]
    connectRouteForNet $block $net1 $routeObjectArray1 $pin1 $stein1

    set point1 [list [expr $xMid-15] [expr $yMid+10]]
    set point2 [list $xMid [expr $yMid+10]]
    set seg2 [oa::oaPathSeg_create $block $lNum $pNum $point1 $point2 $segSty]

    set routeObjectArray2 [list $seg2]

    set instTerm [getInstTerm $orInst $net1 "T1"]

    connectRouteForNet $block $net1 $routeObjectArray2 $stein1 $instTerm

    set point1 [list [expr $xMid-15] [expr $yMid+10]]
    set point2 [list [expr $xMid-15] [expr $yMid2+10]]
    set seg3 [oa::oaPathSeg_create $block $lNum $pNum $point1 $point2 $segSty]

    set point1 [list [expr $xMid-15] [expr $yMid2+10]]
    set point2 [list $xMid [expr $yMid2+10]]
    set seg4 [oa::oaPathSeg_create $block $lNum $pNum $point1 $point2 $segSty]

    set routeObjectArray3 [list $seg3 $seg4]

    set instTerm [getInstTerm $nand1 $net1 "T1"]
    connectRouteForNet $block $net1 $routeObjectArray3 $stein1 $instTerm

    set net2 [createNet $block "N2"]
    set bbox [list 0 0 0 0]
    set stein2 [oa::oaSteiner_create $block $bbox]

    set point1 [list [expr $xMid-50] [expr $yMid-10]]
    set point2 [list [expr $xMid-25] [expr $yMid-10]]
    set seg5 [oa::oaPathSeg_create $block $lNum $pNum $point1 $point2 $segSty]

    set routeObjectArray4 [list $seg5]

    set term2 [createTerm $net2 "b" $oa::oacInputTermType]

    set ptArray2 [list \
                [list [expr $xMid-70] [expr $yMid-5]] \
                [list [expr $xMid-55] [expr $yMid-5]] \
                [list [expr $xMid-50] [expr $yMid-10]] \
                [list [expr $xMid-55] [expr $yMid-15]] \
                [list [expr $xMid-70] [expr $yMid-15]]]

    set pin2 [createPin $block $term2 $ptArray2 "P2-In" $oa::oacLeft]

    connectRouteForNet $block $net2 $routeObjectArray4 $pin2 $stein2


    set point1 [list [expr $xMid-25] [expr $yMid-10]]
    set point2 [list $xMid [expr $yMid-10]]
    set seg6 [oa::oaPathSeg_create $block $lNum $pNum $point1 $point2 $segSty]

    set routeObjectArray5 [list $seg6]

    set instTerm [getInstTerm $orInst $net2 "T2"]

    connectRouteForNet $block $net2 $routeObjectArray5 $stein2 $instTerm

    set point1 [list [expr $xMid-25] [expr $yMid-10]]
    set point2 [list [expr $xMid-25] [expr $yMid2-10]]
    set seg7 [oa::oaPathSeg_create $block $lNum $pNum $point1 $point2 $segSty]

    set point1 $point2
    set point2 [list $xMid [expr $yMid2-10]]
    set seg8 [oa::oaPathSeg_create $block $lNum $pNum $point1 $point2 $segSty]

    set routeObjectArray6 [list $seg7 $seg8]

    set instTerm [getInstTerm $nand1 $net2 "T2"]
    connectRouteForNet $block $net2 $routeObjectArray6 $stein2 $instTerm

    set xPt  [expr $xMid+$width]
    set yPt  $yMid
    set yPt2 $yMid2
    set yPt3 [expr ($yPt+$yPt2)/2]

    set yNand2 [expr ($yPt+$yPt2)/2-$height/2]
    # 0, 0 of nand gate is 65 units inside in x from lower left corner of its bBox.
    set xNand2 [expr $xPt+40+65]

    set trans [oa::oaTransform $xNand2 $yNand2]
    set nand2 [createInstance $block $trans "Nand" "NandInst2" $scNameLib]

    set net3 [createNet $block "N3"]

    set instTerm1 [getInstTerm $orInst $net3 "T3"]
    set instTerm2 [getInstTerm $nand2 $net3 "T1"]

    set point1 [list $xPt $yPt]
    set point2 [list [expr $xPt+20] $yPt]
    set seg9 [oa::oaPathSeg_create $block $lNum $pNum $point1 $point2 $segSty]

    set point1 $point2
    set point2 [list [expr $xPt+20] [expr $yPt3+10]]
    set seg10 [oa::oaPathSeg_create $block $lNum $pNum $point1 $point2 $segSty]

    set point1 $point2
    set point2 [list [expr $xPt+40] [expr $yPt3+10]]
    set seg11 [oa::oaPathSeg_create $block $lNum $pNum $point1 $point2 $segSty]

    set routeObjectArray7 [list $seg9 $seg10 $seg11]

    connectRouteForNet $block $net3 $routeObjectArray7 $instTerm1 $instTerm2

    set net4 [createNet $block "N4"]

    set instTerm1 [getInstTerm $nand1 $net4 "T3"]
    set instTerm2 [getInstTerm $nand2 $net4 "T2"]


    set point1 [list $xPt $yPt2]
    set point2 [list [expr $xPt+20] $yPt2]
    set seg12 [oa::oaPathSeg_create $block $lNum $pNum $point1 $point2 $segSty]

    set point1 $point2
    set point2 [list [expr $xPt+20] [expr $yPt3-10]]
    set seg13 [oa::oaPathSeg_create $block $lNum $pNum $point1 $point2 $segSty]

    set point1 $point2
    set point2 [list [expr $xPt+40] [expr $yPt3-10]]
    set seg14 [oa::oaPathSeg_create $block $lNum $pNum $point1 $point2 $segSty]

    set routeObjectArray8 [list $seg12 $seg13 $seg14]

    connectRouteForNet $block $net4 $routeObjectArray8 $instTerm1 $instTerm2

    set xfto9 [expr $xPt +40+$width]
    set yfto9 [expr ($yPt+$yPt2) / 2]
    set net5 [createNet $block "N5"]

    set point1 [list $xfto9 $yfto9]
    set point2 [list [expr $xfto9+30] $yfto9]
    set seg15 [oa::oaPathSeg_create $block $lNum $pNum $point1 $point2 $segSty]

    set routeObjectArray9 [list $seg15]

    set yInv [expr $yfto9-25]
    # {0, 0} of Inv gate is 35 units inside in x from lower left corner of its bBox.
    set xInv [expr $xfto9+30+35]

    set trans [oa::oaTransform $xInv $yInv]
    set inv [createInstance $block $trans "Inv" "InvInst" $scNameLib]
    # createInstTerm(net5, inv, "T1"]

    set instTerm1 [getInstTerm $nand2 $net5 "T3"]
    set instTerm2 [getInstTerm $inv $net5 "T1"]
    connectRouteForNet $block $net5 $routeObjectArray9 $instTerm1 $instTerm2

    set point1 [list [expr $xfto9+15] $yfto9]
    set point2 [list [expr $xfto9+15] [expr $yfto9-90]]
    set seg16 [oa::oaPathSeg_create $block $lNum $pNum $point1 $point2 $segSty]

    set routeObjectArray10 [list $seg16]

    set danglingTerm [createTerm $net5 "Term-open" $oa::oacOutputTermType]
    connectRouteForNet $block $net5 $routeObjectArray10 "NULL" "NULL"

    set xpt [expr $xfto9+15]
    set ypt [expr $yfto9-90]

    set ptArray3 [list \
                 [list [expr $xpt+5] [expr $ypt-20]] \
                 [list [expr $xpt+5] [expr $ypt-5]] \
                 [list $xpt $ypt] \
                 [list [expr $xpt-5] [expr $ypt-5]] \
                 [list [expr $xpt-5] [expr $ypt-20]]]

    createPin $block $danglingTerm $ptArray3 "P-Out" $oa::oacBottom 

    set xfto10 [expr $xfto9+30+160]
    set yfto10 $yfto9

    set net6 [createNet $block "N6"]

    set xPinPt [expr $xfto10+30]
    set ptArray4 [list \
                 [list [expr $xPinPt+20] [expr $yfto10+5]] \
                 [list [expr $xPinPt+ 5] [expr $yfto10+5]] \
                 [list [expr $xPinPt] $yfto10] \
                 [list [expr $xPinPt+ 5] [expr $yfto10-5]] \
                 [list [expr $xPinPt+20] [expr $yfto10-5]]]

    set term3 [createTerm $net6 "SUM" $oa::oacOutputTermType]
    set pin4 [createPin $block $term3 $ptArray4 "P3-Out" $oa::oacRight]

    set instTerm [getInstTerm $inv $net6 "T2"]

    set point1 [list $xfto10 $yfto10]
    set point2 [list [expr $xfto10+30] $yfto10]
    set seg17 [oa::oaPathSeg_create $block $lNum $pNum $point1 $point2 $segSty]

    set routeObjectArray11 [list $seg17]

    connectRouteForNet $block $net6 $routeObjectArray11 $instTerm $pin4

    createNetShapeRoute $design $block $xMid $yfto9

    $design save
    $design close
}


# *****************************************************************************
# Create an oaPath element between 'start' and 'end'. Also create a
# Rect Fig and oaPin object with the Rect if 'addpin' is true
# *****************************************************************************

proc createFigForNet {design block term net start end pinName {addpin 1}} {
    set lNum ""
    set pNum ""
    getLayerPurpose $design "device" "drawing1" lNum pNum

    set ptArray [list $start]

    set startX [$start x]
    set startY [$start y]

    set endX   [$end   x]
    set endY   [$end   y]

    if {$endX != $startX && $endX > $startX} {
        if {$endY != $startY} {
            lappend ptArray [oa::oaPoint $startX $endY]
        }
    } elseif {$endX != $startX && $endX < $startX} {
        if {$endY != $startY} {
            lappend ptArray [oa::oaPoint $endX $startY]
        }
    }

    lappend ptArray $end
    set path [oa::oaPath_create $block $lNum $pNum 2 $ptArray]
    $path addToNet $net

    if {$addpin} {
        set dir ""
        set pt1 $end
        set pt2 $end
        if {[[$term getTermType] typeEnum] == $oa::oacInputTermType} {
            set pt1 [$pt1 + [list -2 -2]]
            set dir $oa::oacLeft
        } elseif {[[$term getTermType] typeEnum] == $oa::oacOutputTermType} {
            set pt2 [$pt2 + [list 2 2]]
            set dir $oa::oacRight
        }
        set box [oa::oaBox $pt1 $pt2]
        set rect [oa::oaRect_create $block $lNum $pNum $box]
        set pin  [oa::oaPin_create  $term $pinName $dir]
        $rect addToPin $pin
    }
    return $path
}


# ***************************************************************************
# Create a few Nets in the top Block with some Routes associated.
# ***************************************************************************

proc createEmptyNets {top topBlock box} {
    set refx [$box right]
    set refy [$box top]

    # A Net with one terminal but without connection to top
    set net1 [createNet $topBlock "EmptyNet1"]
    createTerm $net1 "TermEmptyNet" $oa::oacInputTermType

    puts "An empty net with one terminal is created"

    # Creating Net that has route->shape->route connection

    set rec_box [list [expr $refx+50] $refy [expr $refx+70] [expr $refy+ 20]]
    set lNum ""
    set pNum ""

    set beginExt 0
    set endExt   0
    set width    2
    getLayerPurpose $top "device" "drawing1" lNum pNum

    set endStyle [oa::oaEndStyle "truncate"]
    set segSty [oa::oaSegStyle $width $endStyle $endStyle $beginExt $endExt]

    set rect [oa::oaRect_create $topBlock $lNum $pNum $rec_box]

    set point1 [list $refx [expr $refy+10]]
    set point2 [list [expr $refx+50] [expr $refy+10]]
    set seg1 [oa::oaPathSeg_create $topBlock $lNum $pNum $point1 $point2 $segSty]

    set routeObjectArray1 [list $seg1]
    set route1 [oa::oaRoute_create $topBlock]
    $route1 setObjects $routeObjectArray1

    set point1 [list [expr $refx+60] [expr $refy+10]]
    set point2 [list [expr $refx+90] [expr $refy+10]]
    set seg2 [oa::oaPathSeg_create $topBlock $lNum $pNum $point1 $point2 $segSty]

    set routeObjectArray2 [list $seg2]
    set route2 [oa::oaRoute_create $topBlock]
    $route2 setObjects $routeObjectArray2

    $route1 setEndConn   $rect
    $route2 setBeginConn $rect

    set routeNet1 [createNet $topBlock "RouteNet1"]
    $rect   addToNet $routeNet1
    $route1 addToNet $routeNet1
    $route2 addToNet $routeNet1

    puts "A net with 2 routes and a shape is created"

    # A net with route->route

    set point1 [list [expr $refx+100] $refy]
    set point2 [list [expr $refx+120] $refy]
    set seg3 [oa::oaPathSeg_create $topBlock $lNum $pNum $point1 $point2 $segSty]

    set routeObjectArray3 [list $seg3]
    set route3 [oa::oaRoute_create $topBlock]
    $route3 setObjects $routeObjectArray3

    set point1 [list [expr $refx+120] [expr $refy-20]]
    set point2 [list [expr $refx+120] [expr $refy+20]]
    set seg4 [oa::oaPathSeg_create $topBlock $lNum $pNum $point1 $point2 $segSty]

    set routeObjectArray4 [list $seg4]
    set route4 [oa::oaRoute_create $topBlock]
    $route4 setObjects $routeObjectArray4

    set point1 [list [expr $refx+120] [expr $refy-10]]
    set point2 [list [expr $refx+130] [expr $refy-10]]
    set seg5 [oa::oaPathSeg_create $topBlock $lNum $pNum $point1 $point2 $segSty]

    set routeObjectArray5 [list $seg5]
    set route5 [oa::oaRoute_create $topBlock]
    $route5 setObjects $routeObjectArray5

    set routeNet2 [createNet $topBlock "RouteNet2"]
    $route3 addToNet $routeNet2
    $route4 addToNet $routeNet2
    $route5 addToNet $routeNet2

    puts "A net with 2 routes connecting to each other is created"
}


# *****************************************************************************
# Creates a hierarchy in the top level Design with cellname "Sample".
# It contains three Inst of the Design with the cellname "Gate" and
# connects the input and output Terms of the gates. Logical and physical
# connections are also created. The Design has 4 input and an 1 output Terms.
# *****************************************************************************

proc createHierarchy {scNameLib} {
    variable mode_write
    variable mode_read

    set ns [oa::oaNativeNS]
    set cell  [oa::oaScalarName $ns "Sample"]
    set cell2 [oa::oaScalarName $ns "Gate"]
    set view  [oa::oaScalarName $ns "schematic"]

    set type [oa::oaViewType_get [oa::oaReservedViewType "schematic"]]

    puts "\nCreating Sample schematic with 3 Gate instances"
    set top [oa::oaDesign_open $scNameLib $cell $view $type $mode_write]
    set topBlock [oa::oaBlock_create $top]

    $top setCellType [oa::oaCellType "softMacro"]
    set master [oa::oaDesign_open $scNameLib $cell2 $view $type $mode_read]
    set masterBlock [$master getTopBlock]

    set bbox [$masterBlock getBBox]

    set width  [$bbox getWidth]
    set height [$bbox getHeight]

    set trans [oa::oaTransform [expr -$height-5] 0 $oa::oacR90]
    set inst1 [createInstance $topBlock $trans "Gate" "Inst1" $scNameLib]

    set trans [oa::oaTransform [expr $height+5] 0 $oa::oacR90]
    set inst2 [createInstance $topBlock $trans "Gate" "Inst2" $scNameLib]

    set trans [oa::oaTransform 0 [expr $width+20]]
    set inst3 [createInstance $topBlock $trans "Gate" "Inst3" $scNameLib]

    set net1  [createNet $topBlock "N1"]
    set term1 [createTerm $net1 "T1" $oa::oacInputTermType]

    set box [$inst1 getBBox]

    set xEnd [$box left]
    set yEnd [$box bottom]

    set point1 [oa::oaPoint [expr $xEnd+20] $yEnd]
    set point2 [oa::oaPoint [expr $xEnd+20] [expr $yEnd-50]]
    createFigForNet $top $topBlock $term1 $net1 $point1 $point2 "P-1"

    set pin [oa::oaPin_find $term1 "P-1"]
    set instTerm [getInstTerm $inst1 $net1 "a"]

    set lNum ""
    set pNum ""
    set beginExt 0
    set endExt   0
    set widthRt  2
    getLayerPurpose $top "device" "drawing1" lNum pNum

    set endStyle [oa::oaEndStyle "truncate"]
    set segSty [oa::oaSegStyle $widthRt $endStyle $endStyle $beginExt $endExt]

    set point1 [list [expr $xEnd+20] $yEnd]
    set point2 [list [expr $xEnd+20] [expr $yEnd-50]]
    set seg1 [oa::oaPathSeg_create $topBlock $lNum $pNum $point1 $point2 $segSty]

    set routeObjectArrayElements [list $seg1]

    connectRouteForNet $topBlock $net1 $routeObjectArrayElements $instTerm $pin

    set net2 [createNet $topBlock "N2"]
    set term2 [createTerm $net2 "T2" $oa::oacInputTermType]
    createInstTerm $net2 $inst1 "b"

    set point1 [oa::oaPoint [expr $xEnd+40] $yEnd]
    set point2 [oa::oaPoint [expr $xEnd+40] [expr $yEnd-50]]
    createFigForNet $top $topBlock $term2 $net2 $point1 $point2 "P-2"

    set net3 [createNet $topBlock "N3"]
    createInstTerm $net3 $inst1 "SUM"

    set center [$box getCenter]

    set box [$inst3 getBBox]

    set xEnd3 [$box left]
    set yEnd3 [$box bottom]

    set center3 [$box getCenter]

    set point1 [$center + [list 0 [expr $width/2]] ]
    set point2 [oa::oaPoint $xEnd3 [expr $yEnd3+$height-20]]
    createFigForNet $top $topBlock "NULL" $net3 $point1 $point2 "" 0

    set net4 [createNet $topBlock "N4"]
    set term3 [createTerm $net4 "T3"  $oa::oacInputTermType]
    createInstTerm $net4 $inst2 "a"

    set box [$inst2 getBBox]
    set xEnd2 [$box left]
    set yEnd2 [$box bottom]

    set center2 [$box getCenter]

    set point1 [oa::oaPoint [expr $xEnd2+20] $yEnd2]
    set point2 [oa::oaPoint [expr $xEnd2+20] [expr $yEnd2-50]]
    createFigForNet $top $topBlock $term3 $net4 $point1 $point2 "P-3"

    set net5 [createNet $topBlock "N5"]
    set term4 [createTerm $net5 "T4" $oa::oacInputTermType]
    createInstTerm $net5 $inst2 "b"

    set point1 [oa::oaPoint [expr $xEnd2+40] $yEnd2]
    set point2 [oa::oaPoint [expr $xEnd2+40] [expr $yEnd2-50]]
    createFigForNet $top $topBlock $term4 $net5 $point1 $point2 "P-4"

    set net6 [createNet $topBlock "N6"]
    createInstTerm $net6 $inst2 "SUM"

    set point1 [$center2 + [list 0 [expr $width/2]]]
    set point2 [oa::oaPoint $xEnd3 [expr $yEnd3+$height-40]]
    createFigForNet $top $topBlock "NULL" $net6 $point1 $point2 "" 0

    createInstTerm $net3 $inst3 "a"
    createInstTerm $net6 $inst3 "b"

    set net7 [createNet $topBlock "N7"]
    set term5 [createTerm $net7 "T5" $oa::oacOutputTermType]
    createInstTerm $net7 $inst3 "SUM"

    set point1 [$center3 + [list [expr $width/2] 0]]
    set point2 [$center3 + [list [expr $width/2+50] 0]]
    createFigForNet $top $topBlock $term5 $net7 $point1 $point2 "P-5"
    createEmptyNets $top $topBlock $box

    $top save
    $top close
}


# *****************************************************************************
# Creates the OR schematic Design.
# *****************************************************************************

proc createOrSchematic {scNameLib} {
    variable mode_write

    set ns [oa::oaNativeNS]
    set cell [oa::oaScalarName $ns "Or"]
    set viewStr [oa::oaScalarName $ns "schematic"]

    set vtSch [oa::oaViewType_get [oa::oaReservedViewType "schematic"]]

    set view [oa::oaDesign_open $scNameLib $cell $viewStr $vtSch $mode_write]
    set block [oa::oaBlock_create $view]

    set layer "pin"
    set purpose "drawing"

    set pinLayer 0
    set pinPurp  0

    set success [getLayerPurpose $view $layer $purpose pinLayer pinPurp]
    assert $success "success"

    set pointArray1 [list [list -50 50] [list 15 50]]

    set path [oa::oaPath_create $block $pinLayer $pinPurp 2 $pointArray1]
    assert [$path isValid] "path"

    set name [oa::oaScalarName $ns "TopTail"]
    set net [oa::oaScalarNet_create $block $name]
    assert [$net isValid] "net"
    $path addToNet $net

    set termName [oa::oaScalarName $ns "T1"]
    set term [oa::oaScalarTerm_create $net $termName $oa::oacInputTermType]
    assert [$term isValid] "term"

    set box [list -65 46 -50 54]
    set rect [oa::oaRect_create $block $pinLayer $pinPurp $box]
    assert [$rect isValid] "rect"

    set pin [oa::oaPin_create $term "IN1" $oa::oacLeft]
    $rect addToPin $pin
    assert [$pin isValid] "pin"

    set pointArray2 [list [list -50 20] [list 15 20]]

    set path [oa::oaPath_create $block $pinLayer $pinPurp 2 $pointArray2]
    assert [$path isValid] "path"

    set name [oa::oaScalarName $ns "BotTail"]
    set net [oa::oaScalarNet_create $block $name]
    assert [$net isValid] "net"
    $path addToNet $net

    set termName [oa::oaScalarName $ns "T2"]
    set term [oa::oaScalarTerm_create $net $termName $oa::oacInputTermType]
    assert [$term isValid] "term"

    set box [list -65 16 -50 24]
    set rect [oa::oaRect_create $block $pinLayer $pinPurp $box]
    assert [$rect isValid] "rect"

    set pin [oa::oaPin_create $term "IN2" $oa::oacLeft]
    $rect addToPin $pin
    assert [$pin isValid] "pin"

    set drLayer 0
    set drPurp  0

    set success [getLayerPurpose $view "device" "drawing2" drLayer drPurp]
    assert $success "success"

    set ptArray3 [list \
                 [list 0 0] \
                 [list 50 0] \
                 [list 80 10] \
                 [list 100 35] \
                 [list 80 60] \
                 [list 50 70] \
                 [list 0 70] \
                 [list 14 55] \
                 [list 20 35] \
                 [list 14 15]]

    set gate [oa::oaPolygon_create $block $drLayer $drPurp $ptArray3]
    assert [$gate isValid] "gate"

    set ptArray5 [list \
               [list 100 35] \
               [list 140 35]]

    set path2 [oa::oaPath_create $block $pinLayer $pinPurp 2 $ptArray5]
    assert [$path2 isValid] "path2"

    set name [oa::oaScalarName $ns "OutTail"]
    set net2 [oa::oaScalarNet_create $block $name]
    assert [$net2 isValid] "net2"

    $path2 addToNet $net2
    set termName [oa::oaScalarName $ns "T3"]
    set term2 [oa::oaScalarTerm_create $net2 $termName $oa::oacOutputTermType]

    set box [list 140 31 155 39]
    set rect2 [oa::oaRect_create $block $pinLayer $pinPurp $box]
    assert [$rect2 isValid] "rect2"

    set pin2 [oa::oaPin_create $term2 "OUT" $oa::oacRight]
    $rect2 addToPin $pin2
    assert [$pin2 isValid] "pin2"

    $view save
    $view close
}


# *****************************************************************************
# Creates the NANE schematic Design
# *****************************************************************************

proc createNandSchematic {scNameLib} {
    variable mode_write

    set ns [oa::oaNativeNS]
    set cell [oa::oaScalarName $ns "Nand"]
    set viewStr [oa::oaScalarName $ns "schematic"]

    set vtSch [oa::oaViewType_get [oa::oaReservedViewType "schematic"]]

    set view [oa::oaDesign_open $scNameLib $cell $viewStr $vtSch $mode_write]
    set block [oa::oaBlock_create $view]

    assert [$view isValid] "view"

    set layer "pin"
    set purpose "drawing"

    set pinLayer 0
    set pinPurp  0

    set success [getLayerPurpose $view $layer $purpose pinLayer pinPurp]
    assert $success "success"

    set drLayer 0
    set drPurp  0

    set success [getLayerPurpose $view "device" "drawing" drLayer drPurp]
    assert $success "success"

    set ptArray1 [list \
                 [list -50 50] \
                 [list   0 50]]

    set line [oa::oaLine_create $block $drLayer $drPurp $ptArray1]
    assert [$line isValid] "line"

    set netName1 [oa::oaScalarName $ns "TopTail"]
    set net [oa::oaScalarNet_create $block $netName1]
    assert [$net isValid] "net"
    $line addToNet $net

    set termName1 [oa::oaScalarName $ns "T1"]
    set term [oa::oaScalarTerm_create $net $termName1 $oa::oacInputTermType]
    assert [$term isValid] "term"

    set box [list -65 46 -50 54]
    set rect [oa::oaRect_create $block $pinLayer $pinPurp $box]
    assert [$rect isValid] "rect"

    set pin [oa::oaPin_create $term "IN1" $oa::oacLeft]
    $rect addToPin $pin
    assert [$pin isValid] "pin"

    set ptArray2 [list \
                 [list -50 20] \
                 [list   0 20]]

    set line [oa::oaLine_create $block $drLayer $drPurp $ptArray2]
    assert [$line isValid] "line"

    set netName2 [oa::oaScalarName $ns "BotTail"]
    set net [oa::oaScalarNet_create $block $netName2]
    assert [$net isValid] "net"
    $line addToNet $net

    set termName2 [oa::oaScalarName $ns "T2"]
    set term [oa::oaScalarTerm_create $net $termName2 $oa::oacInputTermType]
    assert [$term isValid] "term"

    set box [list -65 16 -50 24]
    set rect [oa::oaRect_create $block $pinLayer $pinPurp $box]
    assert [$rect isValid] "rect"

    set pin [oa::oaPin_create $term "IN2" $oa::oacLeft]
    $rect addToPin $pin
    assert [$pin isValid] "pin"

    set ptArray3 [list \
                 [list 0 0] \
                 [list 80 0] \
                 [list 100 20] \
                 [list 100 50] \
                 [list 80 70] \
                 [list 0 70]]

    set gate [oa::oaPolygon_create $block $drLayer $drPurp $ptArray3]
    assert [$gate isValid] "gate"

    set ptArray4 [list \
                 [list 100 40] \
                 [list 100 30] \
                 [list 110 26] \
                 [list 118 35] \
                 [list 110 44]]

    set bubble [oa::oaPolygon_create $block $drLayer $drPurp $ptArray4]
    assert [$bubble isValid] "bubble"

    set ptArray5 [list \
                 [list 118 35] \
                 [list 140 35]]

    set line2 [oa::oaLine_create $block $drLayer $drPurp $ptArray5]
    assert [$line2 isValid] "line2"

    set netName3 [oa::oaScalarName $ns "OutTail"]
    set net2 [oa::oaScalarNet_create $block  $netName3]
    assert [$net2 isValid] "net2"

    $line2 addToNet $net2
    set termName3 [oa::oaScalarName $ns "T3"]
    set term2 [oa::oaScalarTerm_create $net2 $termName3 $oa::oacOutputTermType]

    set box [list 140 31 155 39]
    set rect2 [oa::oaRect_create $block $pinLayer $pinPurp $box]
    assert [$rect2 isValid] "rect"

    set pin2 [oa::oaPin_create $term2 "OUT" $oa::oacRight]
    $rect2 addToPin $pin2
    assert [$pin2 isValid] "pin2"

    $view save
    $view close
}


# *****************************************************************************
# Create the INV schematic Design
# *****************************************************************************

proc createInvSchematic {scNameLib} {
    variable mode_write

    set ns [oa::oaNativeNS]
    set cell [oa::oaScalarName $ns "Inv"]
    set viewStr [oa::oaScalarName $ns "schematic"]

    set type [oa::oaViewType_get [oa::oaReservedViewType "schematic"]]

    set view [oa::oaDesign_open $scNameLib $cell $viewStr $type $mode_write]
    set block [oa::oaBlock_create $view]

    assert [$view isValid] "view"

    set layer "pin"
    set purpose "drawing"

    set pinLayer 0
    set pinPurp  0

    set success [getLayerPurpose $view $layer $purpose pinLayer pinPurp]
    assert $success "success"

    set ptArray1 [list \
                 [list -20 25] \
                 [list   0 25]]

    set path [oa::oaPath_create $block $pinLayer $pinPurp 2 $ptArray1]
    assert [$path isValid] "path"

    set name [oa::oaScalarName $ns "TopTail"]
    set net [oa::oaScalarNet_create $block $name]
    assert [$net isValid] "net"
    $path addToNet $net

    set termName [oa::oaScalarName $ns "T1"]
    set term [oa::oaScalarTerm_create $net $termName $oa::oacInputTermType]
    assert [$term isValid] "term"

    set box [list -35 21 -20 29]
    set rect [oa::oaRect_create $block $pinLayer $pinPurp $box]
    assert [$rect isValid] "rect"

    set pin [oa::oaPin_create $term "IN1" $oa::oacLeft]
    $rect addToPin $pin
    assert [$pin isValid] "pin"

    set drLayer 0
    set drPurp  0

    set success [getLayerPurpose $view "device" "drawing2" drLayer drPurp]
    assert $success "success"

    set ptArray3 [list \
                 [list 0 0] \
                 [list 0 50] \
                 [list 70 25]]

    set gate [oa::oaPolygon_create $block $drLayer $drPurp $ptArray3]
    assert [$gate isValid] "gate"

    set ptArray4 [list \
                 [list 70 30] \
                 [list 70 20] \
                 [list 80 16] \
                 [list 88 25] \
                 [list 80 34]]

    set bubble [oa::oaPolygon_create $block $drLayer $drPurp $ptArray4]
    assert [$bubble isValid] "bubble"

    set ptArray5 [list \
                 [list 88 25] \
                 [list 110 25]]

    set path2 [oa::oaPath_create $block $pinLayer $pinPurp 2 $ptArray5]
    assert [$path2 isValid] "path2"

    set name [oa::oaScalarName $ns "OutTail"]
    set net2 [oa::oaScalarNet_create $block $name]
    assert [$net2 isValid] "net2"

    $path2 addToNet $net2
    set termName [oa::oaScalarName $ns "T2"]
    set term2 [oa::oaScalarTerm_create $net2 $termName $oa::oacOutputTermType]

    set box [list 110 21 125 29]
    set rect2 [oa::oaRect_create $block $pinLayer $pinPurp $box]
    assert [$rect2 isValid] "rect2"

    set pin2 [oa::oaPin_create $term2 "OUT" $oa::oacRight]
    $rect2 addToPin $pin2
    assert [$pin2 isValid] "pin2"

    $view save
    $view close
}



proc openLibrary {argv} {
    set ns [oa::oaUnixNS]
    set lib "NULL"
    set scNameLib [oa::oaScalarName $ns [lindex $argv 0]]
    set strPathLib [lindex $argv 1]
    set strNameLib [$scNameLib get $ns]

    set lib [oa::oaLib_find $scNameLib]
    if {$lib != "NULL"} {
        puts "***ERROR error lib is already open"
    }

    # The following commented out code illustrates how the API locates a lib.defs file.
    # Unfortunately, the results are unpredictable because the current user may or may
    # not have a lib.defs file either in the current directory or in $HOME.
    # It has been commented out so that automated run/check tests can predict the output.
    # If you are a user is interested in this, please feel free to uncomment this section.
    #set strLibDefsFile [oa::oaLibDefList_getDefaultPath]

    #if {$strLibDefsFile == "{}"} {
    #    puts "No libdefs file"
    #} else {
    #    puts "Existing lib.defs file"
    #}

    set ldl [oa::oaLibDefList_get "lib.defs" "w"]

    if {[oa::oaLib_exists $strPathLib]} {
        set lib [oa::oaLib_open $scNameLib $strPathLib]
        puts "Opened existing [$scNameLib get $ns] in $strPathLib"
    } else {
        set lib [oa::oaLib_create $scNameLib $strPathLib [oa::oaLibMode shared] "oaDMFileSys"]
        puts "Created [$scNameLib get $ns] in $strPathLib"
    }
    oa::oaLibDef_create $ldl $scNameLib $strPathLib
    $ldl save
    puts "  Adding def for [$scNameLib get $ns] to $strPathLib to newly created libdefs file\n"

    if {![$lib isValid]} {
        puts "***ERROR lib is not valid"
    }
    if {![oa::oaLib_exists $strPathLib]} {
        puts "***ERROR lib does not exist"
    }
    return $scNameLib
}

# ****************************** MAIN ******************************

if {$argc != 2} {
    puts "\nCommand line arguments required: LibName  LibDirectoryPath\n\n"
    exit 1
}

# The Tcl interpreter does a great job catching errors because it unwinds
# the stack and shows the OA error, the offending line, and the current proc.
# If you don't want this level of detail, or you want to improve the catch
# block, uncomment the lines of code, below.

#if { [catch {
    oa::oaDesignInit

    set scNameLib [openLibrary $argv]

    createInvSchematic $scNameLib
    createOrSchematic $scNameLib
    createNandSchematic $scNameLib
    createGateSchematic $scNameLib
    createHierarchy $scNameLib
#} err ] } {
#    puts "Caught err: $err"
#}

puts "......Normal completion......"
