#!/usr/bin/env tcl
###############################################################################
#
# Copyright (c) 2002-2010  Si2, Inc.  DUNS 62-191-1718
# Copyright 2010 Synopsys, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
###############################################################################
#
#   Acknowledgments
#
#   The code herein uses syntax, concepts, appearance, style, formats, and
#   techniques, that appear in several copyrighted works without explicit
#   permisssion or attribution but in good faith within the limits of fair and
#   legal use to the extent known by the author[s]. Such works include,
#   - Classroom and Computer-Aided Instruction (CAI) projects from various
#     industries, including related pedagogical techniques, exercise and lab
#     formats, lesson-plans, and instructional models.
#   - K&R C
#   - Stroustroup C++
#   - ANSI C++
#   - C Library (UNIX man pages)
#   - The OpenAccess API and Reference Implementation and related materials
#   - The SWIG development tool and related documentation (www.swig.org)
#
#   The following members of the Scripting Languages Working Group, created the
#   initial version of the infrastructure and code to produce these lab code
#   examples based on the original Si2 OpenAccess Tutorial labs:
#
#     Rudy Albachten, Chair
#     James Masters
#     Christian Delbaere
#     Steve Potter
#     Stefan Zager
#     Carl Olson
#     Doug Keller
#     Koushik Kalyanaraman
#     Brandon Barclay
#
##############################################################################
# This program dumps the connectivity and shape data from an OpenAccess
# Design into an ASCII format that can be sorted and diff'ed to compare
# it to the expected dump contents. The output of this command should be
# passed through a "sort" command to allow the results to be deterministic
# (oaIter order cannot be guaranteed).
#
# This version of the lab uses oaTcl.
#
# This attempts to follow the C++ lab as closely as possible.  The output
# format will be slightly different and this script uses a Tcl coding style.
##############################################################################

#set path [file join $env(OA_ROOT) lib linux_rhel30_gcc411_64 opt]
#lappend auto_path $path

package require oa
package require oaTclCompat

namespace eval dumper {

    variable df
    variable dumpName

    proc init {cellstr viewstr} {
        variable df
        variable dumpName
        #
        # Create a dump file named as a concatenation of cell and design names
        #
        set dumpName "$cellstr$viewstr.oaTcl.dump"
        if {[catch {set df [open $dumpName "w"]} err]} {
            error "Can't write file $dumpName"
        }
        puts "Writing dump to $dumpName"
        
    }

    proc log {formatStr args} {
        variable df
        set str [eval [concat format [list $formatStr] $args]]
        puts -nonewline $df $str

        # Uncomment to send a copy to stdout as well.
        #puts -nonewline $str
    }

    proc closeLog {} {
        variable df
        close $df
    }

    proc sortLog {} {
        variable dumpName
        # Sort output to same file name
        exec sort --ignore-case -o $dumpName $dumpName
    }
}

proc dumpCV {block design lib} {
    # Print high-level Design metadata.

    set lstr [oa::getLibName $design]
    set cstr [oa::getCellName $design]
    set vstr [oa::getViewName $design]
    dumper::log "Design %s/%s/%s " $lstr $cstr $vstr

    set ctype [oa::getCellType $design]
    dumper::log "%s %s " [oa::getName [oa::getViewType $design]] [oa::getName $ctype]

    set bbox [oa::getBBox $block]
    dumper::log "BBOX %s\n" $bbox
}

proc dumpInst {block} {
    # Print all the Insts in the Design and all InstTerms on each Inst.

    set iIter [oa::getInsts $block]

    while {[set inst [oa::getNext $iIter]] != ""} {
        set orient [oa::getOrient $inst]
        set iStr [oa::getName $inst]
        set origin [oa::getOrigin $inst]
        set bbox [oa::getBBox $inst]

        set libStr [oa::getLibName $inst]
        set cellStr [oa::getCellName $inst]
        set viewStr [oa::getViewName $inst]

        # Print the data on the Inst
        #
        dumper::log "Inst %s OF %s/%s/%s " $iStr $libStr $cellStr $viewStr
        dumper::log "%s LOC (%s) BBOX %s\n" [oa::getName $orient] $origin $bbox

        set itIter [oa::getInstTerms $inst]

        # Print a line for each InstTerm on this Inst
        #
        while {[set iterm [oa::getNext $itIter]] != ""} {
            set net [oa::getNet $iterm]
            set tStr [oa::getTermName $iterm]

            if {$net != ""} {
                set nStr [oa::getName $net]
            }

            dumper::log "InstTerm %s of %s is on %s\n" $iStr $tStr $nStr
        }
    }
}

proc dumpNet {block} {
    # Print the Nets in the Design and Terms on each Net.

    set nIter [oa::getNets $block]

    while {[set net [oa::getNext $nIter]] != ""} {
        set stype [oa::getSigType $net]
        set nStr [oa::getName $net]

        # Print the Net line
        #
        dumper::log "Net %s %s \n" $nStr [oa::getName $stype]

        # Print a line for each Term on this Net
        #
        set tIter [oa::getTerms $net]
        while {[set term [oa::getNext $tIter]] != ""} {
            set ttype [oa::getTermType $term]
            set tStr [oa::getName $term]

            dumper::log "Term %s on %s type %s" $tStr $nStr [oa::getName $ttype]
            set iterPins [oa::getPins $term]
            while {[set pin [oa::getNext $iterPins]] != ""} {
                dumper::log " Pin=%s" [oa::getName $pin]
            }
            dumper::log "\n"
        }
    }
}

proc dumpShape {block design} {
    # Print all the shapes in the Block, including type and bounding box,
    # plus the name of any Pin or Net associated.

    set sIter [oa::getShapes $block]
    set tech [oa::getTech $design]

    while {[set shape [oa::getNext $sIter]] != ""} {

        # Start the Shape line with its type and bounding box
        #
        set bbox [oa::getBBox $shape]
        dumper::log "Shape %s" [oa::getName [oa::getType $shape]]
        if {[oa::TypeEnum [oa::getType $shape]] == $oa::oacTextType} {
            dumper::log "\"%s\"" [oa::getText $shape]
        }
        dumper::log " at %s" $bbox

        # Add layer and purpose names to the line
        #
        set lnum [oa::getLayerNum $shape]
        set pnum [oa::getPurposeNum $shape]

        if {$tech != ""} {
            set layer [oa::LayerFind $tech $lnum]
            set purp [oa::PurposeFind $tech $pnum]
            if {$layer != ""} {
                set lStr [oa::getName $layer]
            } else {
                set lStr ""
            }
            if {$purp != ""} {
                set pStr [oa::getName $purp]
            } else {
                set pStr ""
            }
            dumper::log " LPP %s/%s" $lStr $pStr
        } else {
            # If there is no Tech, can't bind the Layer and Purpose nums to names;
            # so just print the number pair instead.
            #
            dumper::log " LPP %d/%d" $lnum $pnum
        }

        # Note if this is a Pin Shape.
        #
        if {[oa::hasPin $shape]} {
            set pin [oa::getPin $shape]
            set pStr [oa::getName $pin]
            dumper::log " on pin %s" $pStr
        }

        # Note if this Shape is attached to a Net.
        #
        if {[oa::hasNet $shape]} {
            set net [oa::getNet $shape]
            set nStr [oa::getName $net]
            dumper::log " on net %s" $nStr
        }
        if {[oa::TypeEnum [oa::getType $shape]] == $oa::oacPathSegType && [oa::hasRoute $shape]} {
            dumper::log " ROUTE"
        }

        dumper::log "\n"

    }
}

proc dumpRtPoint {pt key} {
    # Print a begin or an end point for a Route.
    #
    if {$pt == ""} {
        dumper::log "%s NULL " $key
        return
    }
    dumper::log "%s %s" $key [oa::getName [oa::getType $pt]]
    set t [oa::TypeEnum [oa::getType $pt]]
    if {$t == $oa::oacStdViaType || $t == $oa::oacCustomViaType || $t == $oa::oacInstTermType} {
        set instTerm $pt
        dumper::log "\[%s" [oa::getName [oa::getInst $instTerm]]
        dumper::log "/%s\]" [oa::getName [oa::getTerm $instTerm]]
    } elseif {$t == $oa::oacSteinerType} {
    } elseif {$t == $oa::oacPinType} {
        set pin $pt
        dumper::log " %s" [oa::getName $pin]
        dumper::log "\[%s\]" [oa::getName [oa::getTerm $pin]]
    } else {
        set shape $pt
        set bbox [oa::getBBox $shape]
        set lnum [oa::getLayerNum $shape]
        set pnum [oa::getPurposeNum $shape]
        dumper::log " at %s on %d/%d " $bbox $lnum $pnum
    }
}

proc dumpRtElement {relem} {
    # Print a single RouteElement ( PathSeg or Via ).

    if {[oa::TypeEnum [oa::getType $relem]] == $oa::oacPathSegType} {
        set seg $relem
        set ptSt [oa::Point [list 0 0]]
        set ptEn [oa::Point [list 0 0]]
        oa::getPoints $seg ptSt ptEn
        dumper::log "SEG at (%s)(%s) " $ptSt $ptEn
        dumper::log "on %d " [oa::getLayerNum $seg]
    } else {
        # Hope it's a Via -- probably should check that here!
        #
        set via $relem
        set ptOrigin [oa::getOrigin $via]
        dumper::log "VIA %s at (%s) " [oa::getName [oa::getOrient $via]] $ptOrigin
    }
}

proc dumpRoute {block} {
    # Print the Design's Routes, including begin/end objects, segments, and vias.

    set riter [oa::getRoutes $block]

    while {[set rt [oa::getNext $riter]] != ""} {
        set rstat [oa::getRouteStatus $rt]
        set net [oa::getNet $rt]
        dumper::log "Route ST %s " [oa::getName $rstat]
        if {$net != ""} {
            set nStr [oa::getName $net]
            dumper::log "NET %s " $nStr
        }
        set from [oa::getBeginConn $rt]
        set to [oa::getEndConn $rt]
        dumpRtPoint $from "START"
        dumpRtPoint $to "END"
        set relems [oa::getObjects $rt]
        for {set i 0} {$i < [oa::getNumElements $relems]} {incr i} {
            dumpRtElement [oa::getElement $relems $i]
        }
        dumper::log "\n"
    }
}

proc oadump {libName cellName viewName {cvType ""}} {
    oa::DesignInit

    # Parse the library definitions in the local lib.defs file
    #
    set pathLibDefs "./lib.defs"
    if {![oa::exists [oa::File $pathLibDefs]]} {
        error "***Must have $pathLibDefs file"
    }

    oa::LibDefListOpenLibs

    if {[set lib [oa::LibFind $libName]] == ""} {
        error "***Either Lib name=$libName or its path mapping in file $pathLibDefs is invalid."
    }

    if {[catch {
        if {$cvType != ""} {
            set design [oa::DesignOpen $libName $cellName $viewName [oa::ViewTypeGet $cvType] "r"]
        } else {
            set design [oa::DesignOpen $libName $cellName $viewName "r"]
        }
    } err]} {
        error "Can't read Design $libName/$cellName/$viewName (check lib.defs): $err"
    }

    puts "Reading Design $libName/$cellName/$viewName."

    # Initialize the dumper to set up the file I/O
    #
    dumper::init $cellName $viewName

    # Now dump all selected objects in the Design
    #
    set block [oa::getTopBlock $design]

    # Make sure bounding box of parent Design gets updated with shapes
    # in all its levels of hierarchy -- including those that were added
    # to a block while a parent instantiang that block was not open.
    #
    oa::openHier $design 200

    dumpCV $block $design $lib
    dumpInst $block
    dumpNet $block
    dumpShape $block $design
    dumpRoute $block

    dumper::closeLog
    dumper::sortLog
}

proc main {} {
    global argv

    # Check the number of arguments
    #
    if {[llength $argv] < 3} {
        error "Use oadump.tcl libraryName cellName viewName"
    }

    # If the 4th argument was supplied, it is the DesignType.
    #
    set cvType ""
    if {[llength $argv] == 4} {
        set cvType [lindex $argv 3]
    }

    # Get the names from the command line
    # The native namespace is set as the global namepsace on startup.
    # No need to convert the input arguments.
    #
    set libName [lindex $argv 0]
    set cellName [lindex $argv 1]
    set viewName [lindex $argv 2]

    oadump $libName $cellName $viewName $cvType
}

if {[info exists argv]} {
    # run from the command line
    main
}

