#!/usr/bin/env tcl
##############################################################################
# Copyright 2009 Advanced Micro Devices, Inc.
# Copyright 2010 Synopsys, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
##############################################################################

package forget oa

set path [file dirname [info script]]
set libpath [file normalize [file join $path .. ]]
lappend auto_path $libpath

package require oa

proc main {} {
    global argv
    array set opt $argv
    if {![info exists opt(--lib)]} {
        error "Missing --lib"
    } elseif {![info exists opt(--cell)]} {
        error "Missing --cell"
    } elseif {![info exists opt(--view)]} {
        error "Missing --view"
    }

    oa::oaDesignInit

    if {[info exists opt(--libdefs)]} {
        oa::oaLibDefList_openLibs $opt(--libdefs)
    } else {
        oa::oaLibDefList_openLibs
    }
    
    set ns [oa::oaNativeNS]

    set lib_scname [oa::oaScalarName $ns $opt(--lib)]
    set lib [oa::oaLib_find $lib_scname]
    if {$lib == "NULL"} {
        error "Couldn't find lib $opt(--lib)"
    }
    set lib_access [oa::oaLibAccess "read"]
    if {![$lib getAccess $lib_access]} {
        error "lib $opt(--lib): No Access!"
    }

    set cell_scname [oa::oaScalarName $ns $opt(--cell)]
    set view_scname [oa::oaScalarName $ns $opt(--view)]

    set design [oa::oaDesign_open $lib_scname $cell_scname $view_scname "r"]
    if {$design == "NULL"} {
        error "No design!"
    }

    for {set i 0} {$i < 1} {incr i} {
        set topBlock [$design getTopBlock]
        if {$topBlock == "NULL"} {
            error "No top block!"
        }
        oa::foreach shape [$topBlock getShapes] {
            set bbox [$shape getBBox]
            set left [$bbox left]
            set bottom [$bbox bottom]
            set right [$bbox right]
            set top [$bbox top]
            puts "Shape on layer [$shape getLayerNum] with bbox [list $left $bottom $right $top]"
        }
    }
}

main
