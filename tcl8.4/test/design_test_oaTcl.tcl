#!/usr/bin/env tcl
##############################################################################
# Copyright 2009 Advanced Micro Devices, Inc.
# Copyright 2010 Synopsys, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
##############################################################################

set path [file join $env(OA_ROOT) lib linux_rhel30_gcc411_64 opt]
lappend auto_path $path

package require oa

proc main {} {
    global argv
    array set opt $argv
    if {![info exists opt(--lib)]} {
        error "Missing --lib"
    } elseif {![info exists opt(--cell)]} {
        error "Missing --cell"
    } elseif {![info exists opt(--view)]} {
        error "Missing --view"
    }

    oa::DesignInit

    if {[info exists opt(--libdefs)]} {
        oa::LibDefListOpenLibs $opt(--libdefs)
    } else {
        oa::LibDefListOpenLibs
    }
    
    set ns [oa::NativeNS]

    set lib_scname [oa::ScalarName $ns $opt(--lib)]
    set lib [oa::LibFind $lib_scname]
    if {$lib == ""} {
        error "Couldn't find lib $opt(--lib)"
    }
    if {![oa::getAccess $lib [oa::LibAccess $oa::oacReadLibAccess]]} {
        error "lib $opt(--lib): No Access!"
    }

    set cell_scname [oa::ScalarName $ns $opt(--cell)]
    set view_scname [oa::ScalarName $ns $opt(--view)]

    set design [oa::DesignOpen $lib_scname $cell_scname $view_scname "r"]
    if {$design == ""} {
        error "No design!"
    }

    for {set i 0} {$i < 1} {incr i} {
        set topBlock [oa::getTopBlock $design]
        if {$topBlock == ""} {
            error "No top block!"
        }
        set shapes [oa::getShapes $topBlock]
        while {[set shape [oa::getNext $shapes]] != ""} {
            set bbox [oa::getBBox $shape]
            puts "[oa::getName [oa::getType $shape]] on layer [oa::getLayerNum $shape] with bbox $bbox"
        }
    }
}

main
