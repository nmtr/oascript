#!/usr/bin/env tcl
##############################################################################
# Copyright 2009 Advanced Micro Devices, Inc.
# Copyright 2010 Synopsys, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
##############################################################################

package forget oa

set path [file dirname [info script]]
set libpath [file normalize [file join $path .. ]]
lappend auto_path $libpath

package require oa

proc time_isvalid {block num_iter} {
    set timer [oa::oaTimer]
    for {set i 0} {$i < $num_iter} {incr i} {
        $block isValid
    }
    return [$timer getElapsed]
}

proc main {} {

    set warmup_num 1000
    set nums {1000 10000 100000 1000000 10000000 100000000 1000000000}
    set verbose 0

    global argv
    array set opt $argv
    if {![info exists opt(--lib)]} {
        error "Missing --lib"
    } elseif {![info exists opt(--cell)]} {
        error "Missing --cell"
    } elseif {![info exists opt(--view)]} {
        error "Missing --view"
    }

    oa::oaDesignInit

    if {[info exists opt(--libdefs)]} {
        oa::oaLibDefList_openLibs $opt(--libdefs)
    } else {
        oa::oaLibDefList_openLibs
    }
    
    set ns [oa::oaNativeNS]

    set lib_scname [oa::oaScalarName $ns $opt(--lib)]
    set lib [oa::oaLib_find $lib_scname]
    if {$lib == "NULL"} {
        error "Couldn't find lib $opt(--lib)"
    }
    set lib_access [oa::oaLibAccess "read"]
    if {![$lib getAccess $lib_access]} {
        error "lib $opt(--lib): No Access!"
    }

    set cell_scname [oa::oaScalarName $ns $opt(--cell)]
    set view_scname [oa::oaScalarName $ns $opt(--view)]

    set design [oa::oaDesign_open $lib_scname $cell_scname $view_scname "r"]
    if {$design == "NULL"} {
        error "No design!"
    }

    set topBlock [$design getTopBlock]
    if {$topBlock == "NULL"} {
        error "No top block!"
    }

    if {$verbose == 1} {
        puts "Warmup call to isValid ($warmup_num times)..."
    }
    
    time_isvalid $topBlock $warmup_num
    puts [format "%16s %16s" "# Iter" "Elapsed (s)"]
    puts [format "%16s %16s" "------" "-----------"]
    #exec callgrind_control -i=on
    foreach num $nums {
        puts [format "%16d %16.2f" $num [time_isvalid $topBlock $num]]
    }
    #exec callgrind_control -i=off
}

main
