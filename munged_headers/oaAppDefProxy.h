/*
   Copyright 2009 Advanced Micro Devices, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

// This file contains our sneaky fix for oaAppDefs

#if !defined(oaAppDefProxy_P)
#define oaAppDefProxy_P
#include <string>
#ifdef SWIG
%include "std_string.i"
#endif

BEGIN_OA_NAMESPACE

#ifdef SWIG
// IMPORTANT: do not change the name of defNullOK for oaAppObjectDef in this
// header since we need to disable NULL checking using a %typemap in this
// case.
%typemap(in) const OpenAccess_4::oaAppObjectDef *defNullOK (void *argp = NULL, int res = 0) {
    res = SWIG_ConvertPtr($input, &argp, $descriptor, $disown | %convertptr_flags);
    if (!SWIG_IsOK(res)) { 
        %argument_fail(res, "$type", $symname, $argnum); 
    }
    $1 = %reinterpret_cast(argp, $ltype);
    if ($1 && use_isvalid && !$1->isValid()) {
        SWIG_exception_fail(SWIG_ValueError, "in method '$symname', argument $argnum of type '$type' is not a valid oaObject (isValid() returned false).");
    }
}
#endif

class OA_BASE_DLL_API oaAppDefProxy : public oaAppDef
{
public:
  static oaAppDefProxy *findProxy (const oaString &name, oaUInt4 dbType, oaUInt4 dtIndex, oaDomain domain, const oaAppObjectDef *defNullOK = NULL)
  { return reinterpret_cast<oaAppDefProxy*>(find(name, dbType, dtIndex, domain, defNullOK)); }

};

// ----------------------------------------------------------------------------

class OA_BASE_DLL_API oaIntAppDefProxy : public oaAppDefProxy
{
public:
  static oaIntAppDefProxy *getProxy (const oaString &name, oaUInt4 dbType, oaUInt4 dataType, oaDomain domain, oaBoolean persistent, oaInt4 defValue, const oaAppObjectDef *defNullOK = NULL)
  { return reinterpret_cast<oaIntAppDefProxy*>(getInt(name, dbType, dataType, domain, persistent, defValue, defNullOK)); }

  void set (oaObject *object, oaInt4 value)
  { setInt(object, value); }

  oaInt4 get (const oaObject *object)
  { return getInt(object); }

  oaInt4 getDefault () const
  { return getIntDefault(); }
};

// ----------------------------------------------------------------------------

class OA_BASE_DLL_API oaFloatAppDefProxy : public oaAppDefProxy
{
public:
  static oaFloatAppDefProxy *getProxy (const oaString &name, oaUInt4 dbType, oaUInt4 dataType, oaDomain domain, oaBoolean persistent, oaFloat defValue, const oaAppObjectDef *defNullOK = NULL)
  { return reinterpret_cast<oaFloatAppDefProxy*>(getFloat(name, dbType, dataType, domain, persistent, defValue, defNullOK)); }

  void set (oaObject *object, oaFloat value)
  { setFloat(object, value); }

  oaFloat get (const oaObject *object)
  { return getFloat(object); }

  oaFloat getDefault () const
  { return getFloatDefault(); }
};

// ----------------------------------------------------------------------------

class OA_BASE_DLL_API oaStringAppDefProxy : public oaAppDefProxy
{
public:
  static oaStringAppDefProxy *getProxy (const oaString &name, oaUInt4 dbType, oaUInt4 dataType, oaDomain domain, oaBoolean persistent, const oaString &defValue, const oaAppObjectDef *defNullOK = NULL)
  { return reinterpret_cast<oaStringAppDefProxy*>(getString(name, dbType, dataType, domain, persistent, defValue, defNullOK)); }

  void set (oaObject *object, const oaString &value)
  { setString(object, value); }
    
  oaString get (const oaObject *object)
  {
    oaString value;
    getString(object, value);
    return value;
  }
  
  oaString getDefault() const
  { oaString temp; getStringDefault(temp); return temp; }
};

// ----------------------------------------------------------------------------

class OA_BASE_DLL_API oaIntraPointerAppDefProxy : public oaAppDefProxy
{
public:
  static oaIntraPointerAppDefProxy *getProxy (const oaString &name, oaUInt4 dbType, oaUInt4 dataType, oaDomain domain, oaBoolean persistent, const oaAppObjectDef *defNullOK = NULL)
  { return reinterpret_cast<oaIntraPointerAppDefProxy*>(getIntra(name, dbType, dataType, domain, persistent, defNullOK)); }

  void set (oaObject *object, const oaObject *otherObject)
  { setIntra(object, otherObject); }

  oaObject *get (const oaObject *object)
  { return getIntra(object); }
};

// ----------------------------------------------------------------------------

class OA_BASE_DLL_API oaInterPointerAppDefProxy : public oaAppDefProxy
{
public:
    static oaInterPointerAppDefProxy *getProxy (const oaString &name, oaUInt4 dbType, oaUInt4 dataType, oaDomain domain, oaBoolean persistent, const oaAppObjectDef *defNullOK = NULL)
    { return reinterpret_cast<oaInterPointerAppDefProxy*>(getInter(name, dbType, dataType, domain, persistent, defNullOK)); }
    
    void set (oaObject *object, const oaObject *otherObject)
    { setInter(object, otherObject); }

    oaObject *get (const oaObject *object)
    { return getInter(object); }
};

// ----------------------------------------------------------------------------

class OA_BASE_DLL_API oaDataAppDefProxy : public oaAppDefProxy
{
public:
  static oaDataAppDefProxy *getProxy (const oaString &name, oaUInt4 dbType, oaUInt4 dataType, oaDomain domain, oaBoolean persistent, std::string defValue, const oaAppObjectDef *defNullOK = NULL)
  { return reinterpret_cast<oaDataAppDefProxy*>(getData(name, dbType, dataType, domain, persistent, static_cast<oaUInt4>(defValue.size()), reinterpret_cast<const oaByte*>(defValue.data()), defNullOK)); }
  
  void set (oaObject *object, std::string data)
  {
    if (data.size() < getDataSize()) throw "OOPS";
    setData(object, reinterpret_cast<const oaByte *>(data.data()));
  }

  std::string get (const oaObject *object)
  {
    std::string result;
    oaUInt4 len = getDataSize();
    if (len) {
        oaByte *byteData = new oaByte [len];
        getData(object, byteData);
        result.assign(reinterpret_cast<const char *>(byteData), static_cast<size_t>(len));
        delete [] byteData;
    }
    return result;
  }

  std::string getDefault () const
  {
    std::string result;
    oaUInt4 len = getDataSize();
    if (len) {
        oaByte *byteData = new oaByte [len];
        getDataDefault(byteData);
        result.assign(reinterpret_cast<const char *>(byteData), static_cast<size_t>(len));
        delete [] byteData;
    }
    return result;
  }

  oaUInt4 getSize () const
  { return getDataSize(); }
};

// ----------------------------------------------------------------------------

class OA_BASE_DLL_API oaVarDataAppDefProxy : public oaAppDefProxy
{
public:
  static oaVarDataAppDefProxy *getProxy (const oaString &name, oaUInt4 dbType, oaUInt4 dataType, oaDomain domain, oaBoolean persistent, std::string defValue, const oaAppObjectDef *defNullOK = NULL)
  { return reinterpret_cast<oaVarDataAppDefProxy*>(getVarData(name, dbType, dataType, domain, persistent, static_cast<oaUInt4>(defValue.size()), reinterpret_cast<const oaByte*>(defValue.data()), defNullOK)); }
  
  void set (oaObject *object, std::string data)
  { setVarData(object, (oaUInt4)data.size(), (const oaByte*)data.data()); }

  std::string get (const oaObject *object)
  {
    std::string result;
    oaUInt4 len = getVarDataSize(object);
    if (len) {
        oaByte *byteData = new oaByte [len];
        getVarData(object, byteData);
        result.assign(reinterpret_cast<const char *>(byteData), static_cast<size_t>(len));
        delete [] byteData;
    }
    return result;
  }

  oaUInt4 getSize (const oaObject *object)
  { return getVarDataSize(object); }

  std::string getDefault () const
  {
    std::string result;
    oaUInt4 len = getDefaultSize();
    if (len) {
        oaByte *byteData = new oaByte [len];
        getVarDataDefault(byteData);
        result.assign(reinterpret_cast<const char *>(byteData), static_cast<size_t>(len));
        delete [] byteData;
    }
    return result;
  }

  oaUInt4 getDefaultSize () const
  { return getVarDataSizeDefault(); }
};

// ----------------------------------------------------------------------------

class OA_BASE_DLL_API oaTimeAppDefProxy : public oaAppDefProxy
{
public:
  static oaTimeAppDefProxy *getProxy (const oaString &name, oaUInt4 dbType, oaUInt4 dataType, oaDomain domain, oaBoolean persistent, oaTime defValue, const oaAppObjectDef *defNullOK = NULL)
  { return reinterpret_cast<oaTimeAppDefProxy*>(getTime(name, dbType, dataType, domain, persistent, defValue, defNullOK)); }
  
  void set (oaObject *object, oaTime value)
  { setTime(object, value); }

  oaTime get (const oaObject *object)
  { return getTime(object); }

  oaTime getDefault () const
  { return getTimeDefault(); }
};

// ----------------------------------------------------------------------------

class OA_BASE_DLL_API oaDoubleAppDefProxy : public oaAppDefProxy
{
public:
  static oaDoubleAppDefProxy *getProxy (const oaString &name, oaUInt4 dbType, oaUInt4 dataType, oaDomain domain, oaBoolean persistent, oaDouble defValue, const oaAppObjectDef *defNullOK = NULL)
  { return reinterpret_cast<oaDoubleAppDefProxy*>(getDouble(name, dbType, dataType, domain, persistent, defValue, defNullOK)); }
    
  void set (oaObject *object, oaDouble value)
  { setDouble(object, value); }

  oaDouble get (const oaObject *object)
  { return getDouble(object); }

  oaDouble getDefault () const
  { return getDoubleDefault(); }
};

// ----------------------------------------------------------------------------

class OA_BASE_DLL_API oaVoidPointerAppDefProxy : public oaAppDefProxy
{
public:
  static oaVoidPointerAppDefProxy *getProxy (const oaString &name, oaUInt4 dbType, oaUInt4 dataType, oaDomain domain, const oaAppObjectDef *defNullOK = NULL)
  { return reinterpret_cast<oaVoidPointerAppDefProxy*>(getVoid(name, dbType, dataType, domain, defNullOK)); }
  
  void set (oaObject *object, const void *value)
  { setVoid(object, value); }

  void *get (const oaObject *object)
  { return getVoid(object); }
};

// ----------------------------------------------------------------------------

class OA_BASE_DLL_API oaBooleanAppDefProxy : public oaAppDefProxy
{
public:
  static oaBooleanAppDefProxy *getProxy (const oaString &name, oaUInt4 dbType, oaUInt4 dataType, oaDomain domain, oaBoolean persistent, oaBoolean defValue, const oaAppObjectDef *defNullOK = NULL)
  { return reinterpret_cast<oaBooleanAppDefProxy*>(getBoolean(name, dbType, dataType, domain, persistent, defValue, defNullOK)); }

  void set (oaObject *object, oaBoolean value)
  { setBoolean(object, value); }

  oaBoolean get (const oaObject *object)
  { return getBoolean(object); }

  oaBoolean getDefault () const
  { return getBooleanDefault(); }
};

END_OA_NAMESPACE

#endif

