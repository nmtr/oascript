#!/usr/bin/perl
##############################################################################
# Copyright 2009 Advanced Micro Devices, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
##############################################################################

use strict;
use warnings;

use File::Basename;
use FindBin;
use Getopt::Long;

use lib dirname($FindBin::RealBin);
use oa::design;

my %options;
GetOptions(\%options,
	   "libdefs=s",
	   "lib=s",
	   "cell=s",
	   "view=s"
	   );

my $verbose = 0;
my @nums = (1_000, 10_000, 100_000, 1_000_000, 10_000_000, 100_000_000, 1_000_000_000);
my $warmup_num = 1_000;

eval {

    oa::oaDesignInit();

    if (exists($options{libdefs})) {
	&oa::oaLibDefList::openLibs($options{libdefs});
    } else {
	&oa::oaLibDefList::openLibs();
    }

    my $ns = new oa::oaNativeNS;

    my $lib_name = $options{lib} || die "Missing mandatory -lib argument";
    my $cell_name = $options{cell} || die "Missing mandatory -cell argument";;
    my $view_name = $options{view} || die "Missing mandatory -view argument";;

    my $lib_scname = new oa::oaScalarName($ns, $lib_name);
    my $lib = oa::oaLib::find($lib_scname);
    if (!$lib) {
	print "Couldn't find $lib_name.\n";
	exit 1;
    }
    if (!$lib->getAccess(new oa::oaLibAccess($oa::oacReadLibAccess))) {
	print "lib $lib_name: No access!\n";
	exit 1;
    }

    my $cell_scname = new oa::oaScalarName($ns, $cell_name);
    my $view_scname = new oa::oaScalarName($ns, $view_name);

    my $design = oa::oaDesign::open($lib_scname, $cell_scname, $view_scname, "r");
    if (!$design) {
	print "No design!\n";
	exit 1;
    }

    my $topBlock = $design->getTopBlock();
    if (!$topBlock) {
	print "No top block!\n";
	exit 1;
    }

    print("Warmup call to isValid ($warmup_num times)...\n") if ($verbose);
    &time_isvalid($topBlock, $warmup_num);

    printf("%16s %16s\n", '# Iter', 'Elapsed (s)');
    printf("%16s %16s\n", '------', '-----------');
    foreach my $num (@nums) {
        printf("%16d %16.2f\n", $num, &time_isvalid($topBlock, $num));
    }

};

if ($@) {
    print STDERR "Caught exception: $@\n";
}

exit 0;


sub time_isvalid {
    my ($block, $num_iter) = @_;
    my $timer = new oa::oaTimer();
    for (my $i = 0 ; $i < $num_iter ; $i++) {
        $block->isValid();
    }
    return($timer->getElapsed());
}
