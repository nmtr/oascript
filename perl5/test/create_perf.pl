#!/usr/bin/perl
##############################################################################
# Copyright 2009 Advanced Micro Devices, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
##############################################################################

use strict;
use warnings;

use File::Basename;
use FindBin;
use Getopt::Long;

use lib dirname($FindBin::RealBin);
use oa::design;

sub show_time {
    my ($title, $fun) = @_;
    printf("%-20s", $title);
    my $timer = new oa::oaTimer();
    &$fun();
    printf("%4.2f\n", $timer->getElapsed());
}

my @layers = (['poly',    1, 'poly'],
              ['ndiff',   2, 'nDiff'],
              ['pdiff',   3, 'pDiff'],
              ['contact', 4, 'cut'],
              ['metal1',  5, 'metal'],
              ['via1',    6, 'cut']);

my $max_shapes = 1_000_000;
my $unit_size = 500;


my %options;
GetOptions(\%options,
	   "libdefs=s",
	   "lib=s",
	   "cell=s",
	   "view=s"
	   );

eval {

    oa::oaDesignInit();

    if (exists($options{libdefs})) {
	&oa::oaLibDefList::openLibs($options{libdefs});
    } else {
	&oa::oaLibDefList::openLibs();
    }

    my $ns = new oa::oaNativeNS;

    my $lib_name = $options{lib} || die "Missing mandatory -lib argument";
    my $cell_name = $options{cell} || die "Missing mandatory -cell argument";;
    my $view_name = $options{view} || die "Missing mandatory -view argument";;

    my $lib_scname = new oa::oaScalarName($ns, $lib_name);

    # Create the library
    my $lib = oa::oaLib::find($lib_scname);
    if ($lib) {
	print "Library already exists - remove first: $lib_name.\n";
	exit 1;
    }

    $lib = oa::oaLib::create($lib_scname, "./$lib_name", new oa::oaLibMode($oa::oacSharedLibMode), 'oaDMFileSys');
    unless ($lib->getAccess(new oa::oaLibAccess('write'))) {
        print "lib $lib_name: Cannot get write access!\n";
        exit 1;
    }

    # Create the tech in the new library
    my $tech = oa::oaTech::create($lib_scname);
    foreach my $layer (@layers) {
        #print $$layer[0]."\n";
        my $name = $$layer[0];
        my $num = $$layer[1];
        my $mat_str = $$layer[2];
        #print($tech, $name, $num);
        oa::oaPhysicalLayer::create($tech, $name, $num); #, new oa::oaMaterial($mat_str));
    }

    # Use the first layer number in creating shapes on drawing purpose
    my $use_lnum = 1; #$layers[0][1];
    my $use_pnum = $oa::oavPurposeNumberDrawing;

    my $cell_scname = new oa::oaScalarName($ns, $cell_name);
    my $view_scname = new oa::oaScalarName($ns, $view_name);

    # Create a design and top block
    my $view_type = oa::oaViewType::get(new oa::oaReservedViewType('maskLayout'));
    my $design = oa::oaDesign::open($lib_scname, $cell_scname, $view_scname, $view_type, 'w');
    my $block = oa::oaBlock::create($design);
    my $timer;
    my $i;

    # Create a bunch of rectangles
    &show_time('Create rectangles',
        sub {
            for ($i = 1 ; $i < $max_shapes ; $i++) {
                my @box = ($i*$unit_size, 0, ($i+1)*$unit_size, $unit_size);
                oa::oaRect::create($block, $use_lnum, $use_pnum, \@box);
            }
        }
    );
    
    # Create a bunch of paths
    &show_time('Create paths',
        sub {
            for ($i = 1 ; $i < $max_shapes ; $i++) {
                my $st = $i*2*$unit_size;
                #print "st=$st, i=$i, unit_size=$unit_size\n";
                my @pts = ([$st, 0], [$st+$i*$unit_size, 0], [$st+$i*$unit_size, $i*$unit_size]);
                #foreach my $pt (@pts) {print($$pt[0].",".$$pt[1]." ")} ; print "\n";
                oa::oaPath::create($block, $use_lnum, $use_pnum, $unit_size/4, \@pts);
            }
        }
    );

    # Iterate through all shapes in the block
    &show_time('Iterate shapes',
        sub {
            my $shape_iter = new oa::oaIter::oaShape($block->getShapes());
            while (my $shape = $shape_iter->getNext()) { $shape->isValid() }
        }
    );

};

if ($@) {
    print STDERR "Caught exception: $@\n";
}

exit 0;
