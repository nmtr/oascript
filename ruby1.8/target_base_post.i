/* ****************************************************************************

   Copyright 2009 Advanced Micro Devices, Inc.
   Copyright 2010 Intel Corporation

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 ******************************************************************************

   Change Log:
     02/16/2010: Copied from Python bindings (authored by AMD) and modified by
                 Intel Corporation for Ruby.

 *************************************************************************** */

%template(oaComplex_float) OpenAccess_4::oaComplex<OpenAccess_4::oaFloat>;

%include <macros/base_collections.i>
%include <macros/base_iters.i>

%include "ruby_oaString_extensions.i"

%include <munged_headers/oaAppDefProxy.h>

#ifdef OASCRIPT_base
%include <oa_dcast_post.i>
#endif

