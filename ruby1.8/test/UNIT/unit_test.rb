#! /usr/bin/env ruby
###############################################################################
#
# Copyright 2010 Intel Corporation
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
###############################################################################
#
# Change Log:
#   02/16/2010: Initial copy
#
###############################################################################

require 'test/unit'
require 'oa'
include Oa

class BaseTest < Test::Unit::TestCase

  def test_enum
    libtype = OaType.new(OacLibType)
    assert_equal OacLibType, libtype.to_i
    assert_equal 'Lib', libtype.to_s
  end

  def test_scalar_name
    
    # Common setup
    s = "Foo"
    ns = OaNativeNS.new

    # Tests scalar name casting and also using Ruby string in place of OaString
    sn = OaScalarName.new(ns, s)
    assert_equal(sn.to_s, s)
    assert_equal(sn.to_str, s)

    # Tests default name space capability
    assert_equal('a`b', OaScalarName.new('a`b').to_s)
    OaNameSpace.use(OaCdbaNS.new) {
      # CDBA NS assumes backtick (`) is a hierarchy separator (/ in NativeNS).
      a_b = OaScalarName.new('a`b')
      assert_equal('a`b', a_b.to_s)
      assert_equal('a/b', OaNameSpace.use(OaNativeNS.new) { a_b.to_s })
    }
    assert_equal(OaNameSpace.getDefault.class, OaNativeNS.new.class)
    
    # Tests getting of a string back from the scalar name
    sn.get(ns, s_new='')
    assert_equal(s_new, s)

  end

  def test_point

    pt1 = OaPoint.new(10, 20)
    pt2 = OaPoint.new(30, 40)

    # Basic testing
    assert_equal(10, pt1.x)
    assert_equal(20, pt1.y)

    # Adding & subtracting
    pt3 = pt1 + pt2
    pt4 = pt2 - pt1
    assert_equal(40, pt3.x)
    assert_equal(60, pt3.y)
    assert_equal(20, pt4.x)
    assert_equal(20, pt4.y)

    # Negating
    assert_equal(OaPoint.new(-10,-20), -pt1)

    # Extra methods extended for OA Ruby
    pt1_x2 = OaPoint.new(20, 40)
    pt1_d2 = OaPoint.new(5, 10)
    pt1_a = pt1.to_a
    assert_equal(pt1_x2, pt1*2)
    assert_equal(pt1_d2, pt1/2)
    assert_equal([10,20], pt1_a)
    assert_equal([OaPoint,pt1_a].hash, pt1.hash)
    assert_equal(10, pt1[0])
    assert_equal(20, pt1[1])
    assert_equal([10,20], pt1[0..1])
    assert pt2 > pt1
    assert pt1 >= pt1
    assert pt1 < pt2
    assert pt2 <= pt2
    assert pt1.member?(10)
    assert pt1.any? {|e| e > 15}
    assert pt1.all? {|e| e < 30}
    # deprecated: assert_equal(OaPoint.new(100,400), pt1**2)
    # deprecated: assert_equal(OaPoint.new(4,9), OaPoint.new(2,3)**OaPoint.new(2,2))

    # Using array input typemap
    assert_equal(OaPoint.new(20,30), pt1+[10,10])
    assert_equal(OaPoint.new(20,30), pt1+OaPoint.new(10,10))
    assert_equal(OaPoint.new(0,10), pt1-[10,10])
    assert_equal(OaPoint.new(0,10), pt1-OaPoint.new(10,10))

  end

  def test_box

    box1 = OaBox.new(10, 10, 20, 20)
    box1_x2 = OaBox.new(20, 20, 40, 40)
    box1_d2 = OaBox.new(5, 5, 10, 10)

    # Basic testing
    assert_equal(OaPoint.new(10,10), box1.lowerLeft)
    assert_equal(OaPoint.new(20,20), box1.upperRight)
    assert(box1.contains(OaPoint.new(10,10)))
    assert(! box1.contains(OaPoint.new(10,10), false))

    # Adding & Subtracting
    assert_equal(OaBox.new(15, 20, 25, 30), box1+OaPoint.new(5, 10))
    assert_equal(OaBox.new(-10, -30, 0, -20), box1-OaPoint.new(20, 40))
    
    # Extra methods extended for OA Ruby
    assert_equal(box1_x2, box1*2.0)
    assert_equal(box1_d2, box1/2.0)
    assert_equal([OaBox,box1.to_a].hash, box1.hash)
    assert_equal([10,10,20,20], box1.to_a)

    # Using array input typemap
    assert_equal(OaBox.new(1,2,3,4), OaBox.new([1,2], [3,4]))

  end

  def test_orient
    o = OaOrient.new(OacR90)
    # TODO: o2 = OaOrient.new(:R90)
    o2 = OaOrient.new(OacR90)
    assert_equal(o, o2)
    assert_equal(OaOrient.new(OacR180), o+o2)
  end

  def test_transform
    t1 = OaTransform.new(2,3,OacR90)
    t2 = OaTransform.new(4,5,:R180)
    assert_equal(OaTransform.new(2, 2, OacR270), t1+t2)
    assert_equal([2, 3, :R90], t1.to_a)
  end

end

##############################################################################
  
class DesignTest < Test::Unit::TestCase

  def setup
    oaDesignInit
  end

  def test_create_design
    # Create the library
    @libname = 'lib'
    @libpath = './tmp/lib'
    assert(lib = OaLib.create(@libname, @libpath, :shared, 'oaDMFileSys'))

    # Create the design
    @cellname = 'test'
    assert(design = OaDesign.open(@libname, @cellname, 'layout', :maskLayout, 'w'))
    assert(block = OaBlock.create(design))

    # Ensure oaTime is wrapping as a Ruby Time class
    assert_equal Time, design.getCreateTime.class

    # Create a few common shapes
    lay = 7
    drw = OavPurposeNumberDrawing
    assert (rect=OaRect.create(block, lay, drw, [0,0,10000,10000]))
    assert (text=OaText.create(block, lay, drw, 'foo', [0,0], :centerCenter, :R0, :gothic, 1000))
    assert (path=OaPath.create(block, lay, drw, 2000, [[0,0], [0,10000], [10000,10000]]))
    assert (pseg=OaPathSeg.create(block, lay, drw, [0,0], [0,10000], OaSegStyle.new(6000, :truncate, :truncate)))
    assert (poly=OaPolygon.create(block, lay, drw, [[0,0], [-20000,0], [-20000,10000], [-10000,10000], [-10000,20000], [0,20000]]))
    assert (bndy=OaPRBoundary.create(block, [[-50000,-50000], [-50000,50000], [50000,50000], [50000,-50000]]))
    assert (lblk=OaLayerBlockage.create(block, :routing, lay, [[0,0], [10000,0], [10000,-10000], [0,-10000]]))

    # Check that a shape is casted properly
    assert block.getShapes.find {|s| s.is_a?(OaRect)}
    assert path.methods.member?('getPoints')

    # Create sub-block and place in top cell as instance
    assert(sub_design = OaDesign.open(@libname, 'sub_'+@cellname, 'layout', :maskLayout, 'w'))
    assert(sub_block = OaBlock.create(sub_design))
    assert(sub_bndy = OaPRBoundary.create(sub_block, [[0,0], [0,5000], [5000,5000], [5000,0]]))
    assert(inst1 = OaScalarInst.create(block, sub_design, [25000,25000,:MX]))
    sub_design.save

    # Create net/term/pin/pinfig
    cdba = OaCdbaNS.new
    assert(net_a = OaNet.create(block, OaName.new(cdba, 'a')))
    assert(term_a = OaTerm.create(net_a, OaName.new(cdba, 'a')))
    assert(pin_a = OaPin.create(term_a))
    assert(pfig_a = OaRect.create(block, lay, drw, [-50000,40000,-40000,50000]))
    pfig_a.addToPin(pin_a)

    # Loop through shapes in the design - test "each" on oaCollection
    block.getShapes.each do |shape|
      # TODO...
      assert shape
    end

    # Find a rectangle (exercises the inclusion of "Enumerable" for oaCollections
    assert block.getShapes.find {|s| s.getType.to_s == 'Rect'}

    # Loop through shapes in the design - test "each" on oaIter
    block.getShapes.to_iter.each {|shape| assert shape}

    # Try attaching then finding a property...
    libdm = OaLibDMData.open(@libname, 'a')
    strprop = OaStringProp.create(libdm, 'foo', 'bar')
    strprop.getValue(strprop_val='')
    assert_equal 'bar', strprop_val
    strprop2_find = OaProp.find(libdm, 'foo')
    strprop.getValue(strprop_val_find='')
    assert_equal 'bar', strprop_val

    # Check oaTimeStamp (as Fixnum)
    assert(design.getTimeStamp(:designDataType).is_a?(Fixnum))

    # Create AppDef
    int = OaIntAppDef[OaNet].get('int')
    printf "name: %s, default: %s\n", int.getName, int.getDefault
    int.getSession
    OaIntAppDef[OaNet].find("int")

    # Check oaObject equality and hashing
    a = block.getInsts.to_a.first
    b = block.getInsts.to_a.first
    assert_not_nil a
    assert_equal a, b
    assert_equal a.hash, b.hash
    assert_not_equal a, nil
    assert_not_equal a, "foo"

    # Save/close the design
    design.save
    design.close

    # Save a lib.defs
    File.open('lib.defs', 'w') do |outf|
      outf.puts "DEFINE #{@libname} #{@libpath}"
    end
  end

  def test_exception
    assert_raise(OaException) { OaLibAccess.new('readf') }
  end

end

