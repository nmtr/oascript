#! /usr/bin/env/ruby
###############################################################################
#
# Copyright 2009 Advanced Micro Devices, Inc.
# Copyright 2010 Intel Corporation
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
###############################################################################
#
# Change Log:
#   02/16/2010: Copied from Python bindings (authored by AMD) and modified by
#               Intel Corporation for Ruby.
#
###############################################################################

require 'oa'
require 'getoptlong'

include Oa

opts = GetoptLong.new(['--libdefs', GetoptLong::REQUIRED_ARGUMENT],
                      ['--lib', GetoptLong::REQUIRED_ARGUMENT])

libname = nil
libdefs = nil

opts.each do |opt, arg|
  case opt
  when '--libdefs'
    libdefs = arg
  when '--lib'
    libname = arg
  end
end

abort "Missing mandatory --lib argument" unless libname

# TODO: allow string usage instead of scalar name on #find and #open
ns = OaNativeNS.new
libname_sn = OaScalarName.new(ns, libname)

oaDMInit
oaTechInit

if libdefs
  OaLibDefList.openLibs libdefs
else
  OaLibDefList.openLibs
end

techlib_scname = OaScalarName.new(ns, libname)

unless techlib = OaLib.find(techlib_scname)
  abort "Couldn't find #{techlib_name}"
end

abort "lib #{techlib_name}: No access!" unless techlib.getAccess(OacReadLibAccess)

unless tech = OaTech.open(techlib)
  abort "No tech info!"
end

tech.getLayers.each do |layer|
  layer_name = ''
  layer.getName(layer_name)
  puts "layer #{layer.getNumber} = #{layer_name}"
end
