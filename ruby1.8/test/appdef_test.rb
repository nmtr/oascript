#!/usr/bin/env ruby
##############################################################################
#
# Copyright 2009 Advanced Micro Devices, Inc.
# Copyright 2010 Intel Corporation
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
##############################################################################
#
# Change Log:
#   02/07/2010: Copied from Python bindings (authored by AMD) and modified by
#               Intel Corporation for Ruby.
#
###############################################################################

require 'oa'
include Oa

# Test each oa*AppDef by creating it and then returning the name.  The
# getSession() method is called to test inheritance which was a problem
# previously on the oa*AppDef proxies.

## Boolean

boolean = Oa::OaBooleanAppDef[Oa::OaNet].get('boolean')
printf "name: %s, default: %s\n", boolean.getName, boolean.getDefault
boolean.getSession
Oa::OaBooleanAppDef[Oa::OaNet].find("boolean")

## Data

# data = Oa::OaDataAppDef[Oa::OaNet].get('data', 3, 'foo')
# printf "name: %s, default: %s\n", data.getName, data.getDefault
# data.getSession
# Oa::OaDataAppDef[Oa::OaNet].find("data")

## Double

double = Oa::OaDoubleAppDef[Oa::OaNet].get('double')
printf "name: %s, default: %s\n", double.getName, double.getDefault
double.getSession
Oa::OaDoubleAppDef[Oa::OaNet].find("double")

## Float

float = Oa::OaFloatAppDef[Oa::OaNet].get('float')
printf "name: %s, default: %s\n", float.getName, float.getDefault
float.getSession
Oa::OaFloatAppDef[Oa::OaNet].find("float")

## Int

int = Oa::OaIntAppDef[Oa::OaNet].get('int')
printf "name: %s, default: %s\n", int.getName, int.getDefault
int.getSession
Oa::OaIntAppDef[Oa::OaNet].find("int")

## InterPointer

interPointer = Oa::OaInterPointerAppDef[Oa::OaNet].get('interPointer')
printf "name: %s, no default\n", interPointer.getName
interPointer.getSession
Oa::OaInterPointerAppDef[Oa::OaNet].find("interPointer")

## IntraPointer

intraPointer = Oa::OaIntraPointerAppDef[Oa::OaNet].get('intraPointer')
printf "name: %s, no default\n", intraPointer.getName
intraPointer.getSession
Oa::OaIntraPointerAppDef[Oa::OaNet].find("intraPointer")

## String

string = Oa::OaStringAppDef[Oa::OaNet].get('string', 'foobar')
string.getDefault(stringDefault = '')
printf "name: %s, default: %s\n", string.getName, stringDefault
string.getSession
Oa::OaStringAppDef[Oa::OaNet].find("string")

## Time

time = Oa::OaTimeAppDef[Oa::OaNet].get('time')
printf "name: %s, default: %s\n", time.getName, time.getDefault
time.getSession
Oa::OaTimeAppDef[Oa::OaNet].find("time")

## VarData

# varData = Oa::OaVarDataAppDef[Oa::OaNet].get('varData', 3, 'foo')
# printf "name: %s, default: %s\n", varData.getName, varData.getDefault
# varData.getSession
# Oa::OaVarDataAppDef[Oa::OaNet].find("varData")

## VoidPointer

voidPointer = Oa::OaVoidPointerAppDef[Oa::OaNet].get('voidPointer')
printf "name: %s, no default\n", voidPointer.getName
voidPointer.getSession
Oa::OaVoidPointerAppDef[Oa::OaNet].find("voidPointer")
