###############################################################################
#
# Copyright 2010 Intel Corporation
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
###############################################################################
#
# Change Log:
#   05/18/2010 (jamesm): Breaking out from oa.rb (flattening sub-module levels)
#   08/12/2010 (jamesm): Adding OaTransform and OaOrient extensions
#
###############################################################################

require 'oa/mixins'
require 'oa/base.so'
require 'oa/appdefs'

module Oa
  
  class OaObject
    def ==(other)
      (other.is_a?(OaObject)) ? isEqual(other) : false
    end

    alias :eql? :==
    alias :hash :getHashCode

    # Provides a more meaningful inspection of any OpenAccess object by also
    # showing the object type.
    #def inspect
    #  "#<#{self.class}(#{self.getType.to_str})>"
    #end   
  end

  class OaPoint

    include Enumerable
    
    def to_a ; [x, y] ; end
    def [](*args) ; to_a[*args] ; end
    def *(k) ; transform(k,0,pt=OaPoint.new); pt ; end
    def /(k) ; transform(1.0/k,0,pt=OaPoint.new); pt ; end
    def hash ; [OaPoint, to_a].hash ; end
    def eql?(other) ; self == other ; end
    def inspect ; "OaPoint(#{x}, #{y})" ; end
    def each(&blk) ; to_a.each(&blk) ; end
    def length ; 2 ; end

    # Comparison
    def >(other)  ; x>other.x and y>other.y ; end
    def >=(other) ; self==other or self>other ; end
    def <(other)  ; x<other.x and y<other.y ; end
    def <=(other) ; self==other or self<other ; end
    
  end

  class OaBox
    
    include Enumerable
    
    def to_a ; [left, bottom, right, top] ; end
    def [](*args) ; to_a[*args] ; end
    def *(k)  ; transform(k,0,b=OaBox.new) ; b ; end
    def /(k)  ; transform(1/k,0,b=OaBox.new) ; b ; end
    
    def +(val)
      if val.is_a?(Integer)
        OaBox.new(left+val, bottom+val, right+val, top+val)  # skew
      elsif val.is_a?(OaPoint) or (val.is_a?(Array) and val.length == 2)
        transform(val,b=OaBox.new) ; b                       # oaPoint skew
      else
        raise TypeError, "Cannot skew OaBox of object type #{val.class}"
      end
    end
    
    def -(val)
      if val.is_a?(Integer)
        OaBox.new(left-val, bottom-val, right-val, top-val)  # skew
      elsif val.is_a?(OaPoint)
        transform(OaPoint.new(-val.x, -val.y),b=OaBox.new) ; b # oaPoint skew
      elsif (val.is_a?(Array) and val.length == 2)
        transform([-val[0], -val[1]], b=OaBox.new) ; b       # oaPoint/Array skew
      else
        raise TypeError, "Cannot skew OaBox of object type #{val.class}"
      end
    end
    
    def hash ; [OaBox, to_a].hash ; end
    def eql?(other) ; self == other ; end
    def inspect ; "OaBox(l:#{left}, b:#{bottom}, r:#{right}, t:#{top})" ; end
    def each(&blk) ; to_a.each(&blk) ; end
    def length ; 4 ; end

    # Comparison
    def >(other)  ; box_cmp_all?(self, other) {|e1,e2| e1 > e2} ; end
    def >=(other) ; box_cmp_all?(self, other) {|e1,e2| e1 >= e2} ; end
    def <(other)  ; box_cmp_all?(self, other) {|e1,e2| e1 < e2} ; end
    def <=(other) ; box_cmp_all?(self, other) {|e1,e2| e1 <= e2} ; end

    private

    def box_cmp_all?(box1, box2)
      a1 = box1.to_a
      a2 = box2.to_a
      (0..3).all? {|i| yield a1[i], a2[i]}
    end
    
  end

  class OaTransform
    def to_a ; [xOffset, yOffset, orient.getName.to_sym] ; end
    def hash ; [OaTransform, to_a].hash ; end
    def [](*args) ; to_a[*args] ; end
    def eql?(other) ; self == other ; end
    def +(other)
      concat(other, xform=OaTransform.new) ; xform
    end
    def inspect ; "OaTransform(x:#{xOffset}, y:#{yOffset}, o:#{orient})" ; end
  end

  class OaOrient
    def hash ; [OaTransform, to_i].hash ; end
    def eql?(other) ; to_i == other.to_i ; end
    def ==(other) ; eql?(other) ; end
    def inspect ; "OaOrient(#{self})" ; end
    alias :+ :concat
  end

  class OaNameSpace
    # Temporarily override the default namespace
    def OaNameSpace.use(ns)
      OaNameSpace.push(ns)
      result = yield
      OaNameSpace.pop
      result
    end
   end

  class OaScalarName

    def to_str
      # self.get(OaNameSpace.default)
      str = self.get
      str.to_str
    end

    alias :to_s :to_str
    
    def inspect
      "OaScalarName(#{self})"
    end
  end

  class OaBaseIter
    # Mix-in additional methods from the Enumerable module (requires an #each
    # method)
    include Enumerable

    # Easily loop through all objects in an iterator.  Returns the expired
    # iterator (call #reset to reuse if desired)
#    def each
#      self.reset
#      while obj = self.getNext do
#        yield obj
#      end
#      self
#    end
  end

  class OaParam
    def to_s
      oastr = '' # OaString.new
      self.getName(oastr)
      oastr.to_str
    end

    def inspect
      "OaParam(#{self}=#{getVal.inspect})"
    end

    def getVal
      # TODO: Can we use the enum to get the type instead of a string?
      type = self.getType.getName
      case type
      when 'intParam'     : self.getIntVal
      when 'floatParam'   : self.getFloatVal
      when 'stringParam'  : self.getStringVal
      when 'doubleParam'  : self.getDoubleVal
      when 'booleanParam' : self.getBooleanVal
      when 'timeParam'    : self.getTimeVal
      when 'appParam'
        # TODO how to implement this?
        #warn "Cannot use #getVal on appParam yet"
        nil
      end
    end
  end
    
  class OaBaseCollection
    include Oa::OaBaseCollectionMixin
  end

  # Undocumented?
  class OaDatabaseCollection
    include Oa::OaBaseCollectionMixin
  end

  # TODO: need to include the mixin for other collections... oaAppObjectDefCollection,
  # oaBaseMemNetCollection, etc.  It may be better to put an %extend directive in the
  # collection wrapper and write the "each" method using C++ code.  Or maybe there can
  # be some kind of automatically generated Ruby mixin code as part of the SWIG macros
  # for collections and perhaps other types as well.

  class OaType
    
    alias :to_int :to_i
    alias :to_s :getName

    def inspect
      "OaLibType(#{self})"
    end
    
  end

  class OaException
    # Improve the default string version of the OaException class - add the message ID
    def to_s
      sprintf("%s [%d]", getMsg, getMsgId)
    end
  end
  
end
