###############################################################################
#
# Copyright 2010 Intel Corporation
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
###############################################################################
#
# Change Log:
#   02/16/2010: Initial copy
#   05/18/2010: Breaking out sub-modules from here (only Oa module now)
#
###############################################################################

# Shared "mixin" helper methods (between classes)
require 'oa/mixins'

# Core OA API library loading - order is important
require 'oa/common'
require 'oa/base'
require 'oa/plugin'
require 'oa/dm'
require 'oa/tech'
require 'oa/design'
require 'oa/wafer'
require 'oa/util'
