/* ****************************************************************************

   Copyright 2009 Advanced Micro Devices, Inc.
   Copyright 2010 Intel Corporation

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 ******************************************************************************

   Change Log:
     02/16/2010: Copied from Python bindings (authored by AMD) and modified by
                 Intel Corporation for Ruby.

 *************************************************************************** */

%header %{

int
VALUE_to_oaPoint (VALUE langPt, oaPoint& point)
{
    if (LANG_ARRAYP(langPt) && LANG_ARRAY_SIZE(langPt) == 2) {
	VALUE langEle = LANG_ARRAY_INDEX(langPt, 0);
	oaInt4 intVal = (oaInt4) LANG_NUM_TO_LONG(langEle);
	point.x() = intVal;

	langEle = LANG_ARRAY_INDEX(langPt, 1);
	intVal = (oaInt4) LANG_NUM_TO_LONG(langEle);
	point.y() = intVal;

	return SWIG_OK;
    }

    void *argp;

    if (SWIG_IsOK(SWIG_ConvertPtr(langPt, &argp, SWIGTYPE_p_OpenAccess_4__oaPoint,  0)) && argp != NULL) {
	point = *(reinterpret_cast<oaPoint*>(argp));
	return SWIG_OK;
    }

    return SWIG_ERROR;
}

%}

%typemap(in) const OpenAccess_4::oaPoint& (OpenAccess_4::oaPoint tmpPoint)
{
    int res;
    void *argp;
    
    if (SWIG_IsOK((res = VALUE_to_oaPoint($input, tmpPoint))))
	$1 = &tmpPoint;
    else if (SWIG_IsOK((res = SWIG_ConvertPtr($input, &argp, $1_descriptor,  0))) && argp != NULL)
	$1 = reinterpret_cast<$1_basetype *>(argp);
    else
	SWIG_exception_fail(SWIG_ArgError(res), "in method '" "$symname" "', argument " "$argnum"" of type '" "$1_type""'");
}

%typemap(in) OpenAccess_4::oaPoint
{
    int res;
    void *argp;
    
    if (SWIG_IsOK((res = VALUE_to_oaPoint($input, $1))))
	;
    else if (SWIG_IsOK((res = SWIG_ConvertPtr($input, &argp, $1_descriptor,  0))) && argp != NULL)
	$1 = *(reinterpret_cast<$1_basetype *>(argp));
    else
	SWIG_exception_fail(SWIG_ArgError(res), "in method '" "$symname" "', argument " "$argnum"" of type '" "$1_type""'");
}

%typecheck(0) const OpenAccess_4::oaPoint&
{
  void *tmpVoidPtr;
  $1 = ((LANG_ARRAYP($input) && LANG_ARRAY_SIZE($input) == 2) ||
        SWIG_IsOK(SWIG_ConvertPtr($input, &tmpVoidPtr, $1_descriptor, 0)));
}

%typecheck(0) OpenAccess_4::oaPoint = (const OpenAccess_4::oaPoint&);

%typemap(out) OpenAccess_4::oaPoint&
{
    $1_basetype *foo = new $1_basetype(*$1);
    $result = SWIG_NewPointerObj(SWIG_as_voidptr(foo), $1_descriptor, SWIG_POINTER_OWN |  0 );
}

%typemap(out) OpenAccess_4::oaPoint
{
    $&1_type bar = new $1_type($1);
    $result = SWIG_NewPointerObj(SWIG_as_voidptr(bar), $&1_descriptor, SWIG_POINTER_OWN |  0 );
}
