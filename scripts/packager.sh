#!/bin/sh

if [ -z $1 ]; then
    echo "Usage: packager.sh <version> [<path to make.variables>]";
    exit 1;
fi

version=$1
makevars=$2
reldate=`date +%Y.%m.%d`
relname=oaScript-${version}-${reldate}

if [ $version == "latest" ]; then
    repos=svn+ssh://oacscripting-svn.si2.org/srv/svn/repos/amd-init
else
    repos=svn+ssh://oacscripting-svn.si2.org/srv/svn/repos/amd-init-tags/${version}
fi

echo "Downloading source code for ${relname}..."
if ! svn co ${repos} ${relname}; then
    exit 1;
fi

echo "Creating ${relname}/Changelog"
if ! svn log ${relname} > ${relname}/Changelog; then
    exit 1;
fi

find ${relname} -name .svn -exec rm -rf {} \; -prune

# remove csharp and related files (since they are not ready for release)
cd ${relname}
for file in CMakeLists.txt README_CMake.txt cmake csharp3.0 */CMakeLists.txt
do
    echo "Removing $file"
    rm -rf $file
done
cd ..

echo "Creating ${relname}-src.tar.gz..."
if ! tar cfz ${relname}-src.tar.gz ${relname}; then
    exit 1;
fi

if [ -z ${makevars} ]; then
    makevars=${relname}/make.variables.template;
fi

echo "Using settings from ${makevars}"
if ! cp ${makevars} ${relname}/make.variables; then
    exit 1;
fi

echo "Building wrappers..."
if ! make -C ${relname} wrappers; then
    exit 1;
fi

echo "Creating ${relname}-tu.tar.gz"
if ! tar cfz ${relname}-tu.tar.gz \
        ${relname}/README \
        ${relname}/Changelog \
        ${relname}/Apache_LICENSE-2.0.txt \
        ${relname}/copyright.txt \
        ${relname}/*/README \
        ${relname}/tu.cc.* \
        ; then
    exit 1;
fi

# Temporarily move the tu dump files out of the way
# Create empty copies of them to include in the wrappers tarball
if ! mkdir ${relname}/tmp; then
    exit 1;
fi

if ! mv ${relname}/tu.cc.001t.tu ${relname}/tmp; then
    exit 1;
fi

if ! mv ${relname}/tu.cc.t00.tu ${relname}/tmp; then
    exit 1;
fi

if ! touch -r ${relname}/tmp/tu.cc.001t.tu ${relname}/tu.cc.001t.tu; then
    exit 1;
fi

if ! touch -r ${relname}/tmp/tu.cc.t00.tu ${relname}/tu.cc.t00.tu; then
    exit 1;
fi

echo "Creating ${relname}-wrappers.tar.gz"
if ! tar cfz ${relname}-wrappers.tar.gz \
        ${relname}/README \
        ${relname}/Changelog \
        ${relname}/Apache_LICENSE-2.0.txt \
        ${relname}/copyright.txt \
        ${relname}/*/README \
        ${relname}/*/*_wrap.cc \
        ${relname}/tu.cc.001t.tu \
        ${relname}/tu.cc.t00.tu \
        ${relname}/perl5/oa.pm \
        ${relname}/perl5/oa \
        ${relname}/python2.5/oa \
        ${relname}/macros/.macros \
        ${relname}/munged_headers/.munged_headers \
        ; then
    exit 1;
fi

echo "Building binaries..."
if ! make -C ${relname}; then
    exit 1;
fi

# Create symlinks for alternate minor releases of Perl
cd ${relname}/perl5
ln -s 5.8.5 5.8.0
ln -s 5.8.5 5.8.1
ln -s 5.8.5 5.8.2
ln -s 5.8.5 5.8.3
ln -s 5.8.5 5.8.4
ln -s 5.8.5 5.8.6
ln -s 5.8.5 5.8.7
ln -s 5.8.5 5.8.8
ln -s 5.8.5 5.8.9
cd ../..

echo "Creating ${relname}-bin.tar.gz"
if ! tar cfz ${relname}-bin.tar.gz \
        ${relname}/README \
        ${relname}/Changelog \
        ${relname}/Apache_LICENSE-2.0.txt \
        ${relname}/copyright.txt \
        ${relname}/*/README \
        ${relname}/*/oa \
        ${relname}/perl5/5.8.* \
        ${relname}/perl5/oa.pm \
        ${relname}/perl5/oa \
        ${relname}/ruby1.8/oa.rb \
        ${relname}/tcl8.4/pkgIndex.tcl \
        ; then
    exit 1;
fi

echo "Done!"

