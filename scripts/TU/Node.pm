package TU::Node;

use strict;
use warnings;

use TU;

our %OPERATORS = (
    'and' => '&',
    'andassign' => '&=',
    'assign' => '=',
    'call' => '()',
    'delete' => ' delete',
    'deref' => '*',
    'eq' => '==',
    'ge' => '>=',
    'gt' => '>',
    'le' => '<=',
    'lnot' => '!',
    'lshift' => '<<',
    'lt' => '<',
    'minus' => '-',
    'minusassign' => '-=',
    'ne' => '!=',
    'new' => ' new',
    'not' => '!',
    'or' => '|',
    'orassign' => '|=',
    'plus' => '+',
    'plusassign' => '+=',
    'postdec' => '++',
    'postinc' => '--',
    'predec' => '++',
    'preinc' => '--',
    'ref' => '->',
    'rshift' => '>>',
    'subs' => '[]',
    'vecdelete' => ' delete[]',
    'vecnew' => ' new[]',
    'xor' => '^',
    'xorassign' => '^='
    );
    

sub new {
    my ($pkg, $num, $type, $fields, $parser) = @_;
    my $self = [ $num, $type, $fields, $parser ];
    return bless $self, $pkg;
}

sub getNum {
    my ($self) = @_;
    return $self->[0];
}

sub getType {
    my ($self) = @_;
    return $self->[1];
}

sub getKeys {
    my ($self) = @_;
    return sort keys %{$self->[2]};
}

sub getField {
    my ($self, $key) = @_;
    $key =~ s/\s+$//;
    return unless $key && exists($self->[2]->{$key});
    return $self->[2]->{$key};
}

sub getFields {
    my ($self) = @_;
    return sort keys %{$self->[2]};
}

sub getNotes {
    my ($self) = @_;
    return unless exists($self->[2]->{note});
    return @{$self->[2]->{note}};
}

sub getFunctionArgs {
    my ($self) = @_;
    if ($self->getType() == TU::FUNCTION_DECL) {
        my @args;
        my $arg_ref = $self->getRef('args');
        while ($arg_ref) {
            push(@args, $arg_ref);
            $arg_ref = $arg_ref->getRef('chan');
        }
        return @args;
    }
    else {
        return;
    }
}

sub getParser {
    my ($self) = @_;
    return $self->[3];
}

sub toString {
    my ($self) = shift;
    return sprintf("<@%d:%d %s>", $self->[0], $self->[1], join(' ', map { "$_=" . $self->[2]->{$_} } keys %{$self->[2]}));
}

sub getRef {
    my ($self, $field) = @_;
    return $self->[3]->getNode($self->getField($field));
}

sub getBaseNames {
    my ($self) = @_;
    my @base_names;
    if ($self->[1] == TU::RECORD_TYPE) {
        my $base_arr = $self->getField('base');
        if ($base_arr) {
            for my $base (@$base_arr) {
                push(@base_names, $self->[3]->getNode($$base[0])->getName());
            }
        }
    }

    return @base_names;
}

sub getLastBase {
    my ($self) = @_;
    if ($self->[1] == TU::RECORD_TYPE) {
        my $fields_arr = $self->getField('base');
        if ($fields_arr) {
            return($self->[3]->getNode($$fields_arr[-1][0]));
        }
    }
    return;
}

sub getName {
    my ($self) = @_;
    if ($self->getType() == TU::IDENTIFIER_NODE) {
	return $self->getField('strg');
    }
    elsif ($self->getType() == TU::REFERENCE_TYPE) {
        return $self->getRef('refd')->getName();
    }
    elsif ($self->getType() == TU::POINTER_TYPE) {
        return $self->getRef('ptd ')->getName();
    }
    elsif ($self->getType() == TU::FUNCTION_TYPE) {
        return (($self->getRef('name')) ? $self->getName() : '<anon-func>');
    }
    my $name = $self->getRef('name');
    return unless $name;
    my $nametype = $name->getType();
    if ($nametype == TU::IDENTIFIER_NODE) {
	my $strg = $name->getField('strg');
	return $strg if $strg;
	if ((grep { $_ eq 'operator' } $name->getNotes()) && ($self->getType() == TU::FUNCTION_DECL)) {
	    for my $note ($self->getNotes()) {
		next unless ($note =~ /^operator\s*(\S*)$/);
		return 'operator' . ($1 && exists($OPERATORS{$1}) ? $OPERATORS{$1} : '');
	    }
	}	    
    } elsif ($nametype == TU::TYPE_DECL || $nametype == TU::ENUMERAL_TYPE) {
	return $name->getName();
    }
    return;
}

sub getScope {
    my ($self) = @_;
    if ($self->[1] == TU::RECORD_TYPE) {
	return $self->getRef('name')->getScope();
    }
    my $scope = $self->getRef('scpe');
    return unless $scope;
    my @result = $scope->getScope();
    push(@result, $scope->getName());
    return wantarray ? @result : join('::', @result);
}

sub getFQN {
    my ($self) = @_;

    my @result;

    if ($self->[1] == TU::RECORD_TYPE || $self->[1] == TU::ENUMERAL_TYPE) {
	@result = $self->getRef('name')->getFQN();
    } elsif ($self->[1] == TU::POINTER_TYPE) {
	if ($self->getField('name')) {
	    @result = $self->getFQN($self->getRef('name'));
	} else {
	    @result = $self->getRef('ptd ')->getFQN();
	    $result[-1] .= '*';
	}
    } elsif ($self->[1] == TU::REFERENCE_TYPE) {
	if ($self->getField('name')) {
	    @result = $self->getFQN($self->getRef('name'));
	} else {
	    @result = $self->getRef('refd')->getFQN();
	    $result[-1] .= '&';
	}
    } else {
	@result = $self->getScope();
	push(@result, $self->getName());
    }

    return wantarray ? @result : join('::', @result);
}

sub getTypeHash ($$) {
    my ($self) = @_;

    my %result = ('name'  => '?',
                  'ptr'   => 0,
                  'ref'   => 0,
                  'const' => 0);

    my $nodetype = $self->getType();
    if ($nodetype == TU::POINTER_TYPE) {
        $result{'name'} = $self->getRef('ptd ')->getName();
        $result{'ptr'} = 1;
    } elsif ($self->getType() == TU::REFERENCE_TYPE) {
	$result{'name'} = $self->getRef('refd')->getName();
        $result{'ref'} = 1;
    } else {
	$result{'name'} = $self->getName();
    }

    return unless ($result{'name'});

    $result{'const'} = $self->isConstType();

    return %result;
}

sub isConstType($$) {

    my ($self) = @_;

    my $nodetype = $self->getType();
    if ($nodetype == TU::POINTER_TYPE) {
	return($self->getRef('ptd ')->isConstType());
    } elsif ($self->getType() == TU::REFERENCE_TYPE) {
	return($self->getRef('refd')->isConstType());
    } else {
        my $qual = $self->getField('qual');
	return($qual && $qual eq 'c');
    }
    
}

sub getTypeString ($$) {
    my ($self, $nofqn) = @_;

    my $result;

    my $nodetype = $self->getType();
    if ($nodetype == TU::POINTER_TYPE) {
	$result = $self->getRef('ptd ')->getTypeString($nofqn) . ' *';
    } elsif ($self->getType() == TU::REFERENCE_TYPE) {
	$result = $self->getRef('refd')->getTypeString($nofqn) . ' &';
    } else {
	$result = $nofqn ? $self->getName() : $self->getFQN();
    }

    return unless $result;

    my $qual = $self->getField('qual');
    if ($qual && $qual eq 'c') {
	$result .= ' const';
    }

    return $result;
}


1;
