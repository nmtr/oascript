# Cmake CMakeLists.txt control file for OpenAccess Scripting
# Languages Working Group Python binding tests
#
# WARNING: GNU Make is the officially supported build environment for oaScript.
# Build oaScript according to the instructions in the README file.
#
# Copyright 2011 Voom, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


# Find and set up other packages
#     (Nothing for now)
# Done finding and setting up other packages


# Unit tests

# TODO: testLib is a prerequisite of tech and design, but that's not enforced here
add_test(${PROJECT_NAME}Test_testLib ${PYTHON_EXECUTABLE} ${CMAKE_CURRENT_SOURCE_DIR}/create_perf.py --lib testLib --cell testCell --view layout --nshapes=10)
add_test(${PROJECT_NAME}Test_base    ${PYTHON_EXECUTABLE} ${CMAKE_CURRENT_SOURCE_DIR}/base_test.py)
add_test(${PROJECT_NAME}Test_perf    ${PYTHON_EXECUTABLE} ${CMAKE_CURRENT_SOURCE_DIR}/create_perf.py --lib perfLib --cell perfCell --view layout)
add_test(${PROJECT_NAME}Test_dm      ${PYTHON_EXECUTABLE} ${CMAKE_CURRENT_SOURCE_DIR}/dm_test.py)
add_test(${PROJECT_NAME}Test_design  ${PYTHON_EXECUTABLE} ${CMAKE_CURRENT_SOURCE_DIR}/design_test.py --lib testLib --cell testCell --view layout)
add_test(${PROJECT_NAME}Test_tech    ${PYTHON_EXECUTABLE} ${CMAKE_CURRENT_SOURCE_DIR}/tech_test.py --lib testLib)
add_test(${PROJECT_NAME}Test_appdef  ${PYTHON_EXECUTABLE} ${CMAKE_CURRENT_SOURCE_DIR}/appdef_test.py)
