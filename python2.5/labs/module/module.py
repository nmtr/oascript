#!/usr/bin/env python2.5
################################################################################
#  Copyright {c} 2002-2010  Si2, Inc.  DUNS 62-191-1718
#
#  Licensed under the Apache License, Version 2.0 {the "License" you may not use
#  this file except in compliance with the License. You may obtain a copy of the
#  License at http:#www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software distributed
#  under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
#  CONDITIONS OF ANY KIND, either express or implied. See the License for the
#  specific language governing permissions and limitations under the License.
#
################################################################################
#  Version History
#
#   DATE      VERSION  BY               DESCRIPTION
#   05/02/11  10.0     bpfeil, Si2     Tutorial 10th Edition
#
################################################################################
#
#   Acknowledgments
#
#   The code herein uses syntax, concepts, appearance, style, formats, and
#   techniques, that appear in several copyrighted works without explicit
#   permisssion or attribution but in good faith within the limits of fair and
#   legal use to the extent known by the author[s]. Such works include,
#   - Classroom and Computer-Aided Instruction {CAI} projects from various
#     industries, including related pedagogical techniques, exercise and lab
#     formats, lesson-plans, and instructional models.
#   - K&R C
#   - Stroustroup C++
#   - ANSI C++
#   - C Library {UNIX man pages}
#   - The OpenAccess API and Reference Implementation and related materials
#   - The SWIG development tool and related documentation {www.swig.org}
#
#   The following members of the Scripting Languages Working Group, created the
#   initial version of the infrastructure and code to produce these lab code
#   examples based on the original Si2 OpenAccess Tutorial labs:
#
#     Rudy Albachten, Chair
#     James Masters
#     Christian Delbaere
#     Steve Potter
#     Stefan Zager
#     Carl Olson
#     Doug Keller
#     Koushik Kalyanaraman
#     Brandon Barclay
#
################################################################################

import sys
import os
here = os.path.abspath(sys._getframe().f_code.co_filename)
sys.path.append(os.path.join(os.path.dirname(here), '../..'))

import oa.design

# inport the dumpUtils.py utility functions
sys.path.append("../emhlister")

from dumpUtils import * 


#     This program creates a simple, hierarchical design: an adder out of
#     2 half-adders, each of which comprises an AND and XOR gate.
#                           _____           ____            _____
#     Leaf cells:       A--|     |      A--|    |       A--|     |
#     from netlist         | AND |--Y      | OR |--Y       | XOR |--Y
#     lab library       B--|_____|      B--|____|       B--|_____|
#
#
#     Embeded Modules created in this code:
#
#     Half-adder block:            Adder block:                           Co|
#         ____________________            __________________________________|__
#        |                    |          |                                  |  |
#        |          ______    |          |   _____       H1c         _____  |  |
#     A--|-----o---|      |   |       A--|--|     |-----------------|     | |  |
#        |     |   | AND1 |---|--C       |  | HA1 | H1s  _____  H2c | OR1 |-o  |
#        |  o------|______|   |       B--|--|_____|-----|     |-----|_____|    |
#        |  |  |              |          |              | HA2 |                |
#        |  |  |              |          |            o-|_____|----------------|--S
#        |  |  |    _____     |          |            |                        |
#        |  |  o---|      |   |          |            |_____________________   |
#        |  |      | XOR1 |---|--S       |__________________________________|__|
#     B--|--o------|______|   |                                             |
#        |                    |                                             |Ci
#        |____________________|
#
#
#     3-bit Top Module:
#
#               B0  A0           B1  A1           B2  A2
#                |  |             |  |             |  |
#        ________|__|_____________|__|_____________|__|______
#       |    ____|__|__       ____|__|__       ____|__|__    |
#       |   |    B  A  |     |    B  A  |     |    B  A  |   |
#       |   |          |     |          |     |          |   |
#       |   |   Add0   |     |   Add1   |     |   Add2   |   |
#       |   |          | C01 |          | C12 |          |   |
#  Ci---|---|Ci      Co|-----|Ci      Co|-----|Ci      Co|---|---Co
#       |   |___S______|     |___S______|     |___S______|   |
#       |_______|________________|________________|__________|
#               |                |                |
#               |                |                |
#               S0               S1               S2

# -------------------------------- globals ------------------------------
mode_trunc = "w"
mode_read  = "r"
mode_append  = "a"
visibleToBlock = 1


# -------------------------------- subroutines ------------------------------



def new_section (id):
    pstr = "\n\n||||||||||||||||||||||||||||||||||||||||||| ";
    pstr += id
    pstr += " |||||||||||||||||||||||||||||||||||||||||||\n\n"
    sys.stdout.write(pstr)

def assertq (condition, statement):
    pstr = "  ASSERT ["
    if not condition:
         pstr += "FAIL] %s\n" % (statement)
    else:
         pstr += "PASS] %s\n" % (statement)
    sys.stdout.write(pstr)


def get_mapped_name( obj ):
    # Many of the getName functions allow you to enter
    #     $myObj getName
    # to get a simple string representing the Object's name.
    #
    # For those that don't, this function does the ScalarName setup and
    # retrieval, and returns the simple string.

    ns = oa.oaNativeNS()
    scalarName = oa.oaScalarName( ns, " ")

    simpleString = ""

    obj.getName( scalarName )
    simpleString = scalarName.get()
    return simpleString



def getLCVName( obj ):
    # Templatize a function that exploits the commonality of method names to
    # get the Lib/Cell/View name attributes from different types of containers.

    # Return a String that is the concatenation of the Lib/Cell/View names,
    # separated by the / character as a delimiter, using method[s] that are
    # are valid for all possible Inst types the template parameter might designate.
    str = "%s/%s/%s" % ( obj.getLibName(), obj.getCellName(), obj.getViewName() )
    return str


def getNumTerms ( module ):
    # Return the number of ModTerms in a Module.
    return module.getTerms().getCount()



def get_num_instTerms ( module  ):
    # Return the number of ModInstTerms in a Module.
    return module.getInstTerms().getCount()



def get_num_nets ( module ):
    # Return the number of ModNets in a Module
    return module.getNets().getCount()



def create_modInstTerm_by_name( inst, name, net ):
    # Set the mod variable to the master Module of the Inst argument.
    mod  = inst.getMasterModule()

    # A Block-specific Design will not have a master Module, so verify
    # mod exists before trying to use the handle to find its ModTerm ons.
    #
    
    instTerm=''

    if ( mod and mod.isValid()):
        
        # Find the term with the name specified in the function argument.
        term  = oa.oaModTerm.find( mod, name )

        # Verify the handle is not null {ie, there was a Term by that name}.
        #
        if term.isValid():

            # Set the instTerm variable to a ModInstTerm created from the term handle.
            instTerm = oa.oaModInstTerm.create( net, inst, term)

            pstr = "Created InstTerm of \"%s\" on Inst \"%s\",attached to net \"%s\"\n" % (name, inst.getName(), net.getName() )
            log (pstr)

            assertq( eval('instTerm.isValid()' ),'instTerm.isValid() ')

        else:
            pstr =  "***Couldn't find term named '$name' on mod. Quitting.\n"
            sys.stdout.write(pstr)
            sys.exit(2)

    return instTerm



def expect_modTerm_on_net( term, net ):
    # Create an oaCollection named collTermsOnNet consisting of the
    # ModTerms on the ModNet passed as a function argument.
    collTermsOnNet = net.getTerms()

    assertq( eval('collTermsOnNet.getCount() == 1' ), 'collTermsOnNet.getCount() == 1')

    # The only Term in the collection should be the one passed in as an arg.
    for gotTerm in collTermsOnNet:
        assertq (eval('gotTerm.getName() == term.getName()'), 'gotTerm.getName() == term.getName()') 


def create_modNet( module, netName ):
    # Create a ModScalarNet such that the following assertqions pass and
    # a corresponding Net is automatically created in the Block Domain.
    net = oa.oaModScalarNet.create( module, netName )

    assertq ( eval( 'net.isValid()' ), 'net.isValid()')
    assertq ( eval( 'net.getModule().getName() == module.getName()' ), 'net.getModule().getName() == module.getName()' )
    assertq ( eval( 'net.getName() == netName' ), 'net.getName() == netName' )
    assertq ( eval( 'net.getType().getName() == "ModScalarNet"' ), 'net.getType().getName() == "ModScalarNet"')
    assertq ( eval( 'net.getSigType().getName() == "signal"' ), 'net.getSigType().getName() == "signal"')
    assertq ( eval( 'not net.isGlobal()' ),'not net.isGlobal()')
    assertq ( eval( 'not net.isImplicit()' ),'not net.isImplicit() ')
    assertq ( eval( 'net.isEmpty()' ),'net.isEmpty()' )
    assertq ( eval( 'net.getEquivalentNets().isEmpty()' ), 'net.getEquivalentNets().isEmpty()')
    assertq ( eval( 'net.getInstTerms().isEmpty()' ), 'net.getInstTerms().isEmpty()' )
    assertq ( eval( 'net.getTerms().isEmpty()' ), 'net.getTerms().isEmpty()')
    pstr = "Created ModScalarNet '%s'\n" % ( netName )
    log(pstr) 

    return net



def add_modInstTerm ( module, name, ioDir ):
    global ns
    # Use the create_modNet helper function defined above to create a ModScalarNet
    # using the passed arguments. Then create a ModScalarTerm (with the proper
    # OA API function) with attributes specified by the passed arguments, so that
    # appropriate Objects are propagated to the Block Domain and the assertqions pass.
    net  = create_modNet( module, name )
#>>>    name_sc = oa.oaScalarName(ns, name ) 

    term = oa.oaModScalarTerm.create( net, name, oa.oaTermType(ioDir))


    assertq ( eval( 'net.getModule().getName() == module.getName()' ), 'net.getModule().getName() == module.getName()' )
    assertq ( eval( 'net.getType().getName() == "ModScalarNet"' ), 'net.getType().getName() == "ModScalarNet"')
    assertq ( eval( 'net.getSigType().getName() == "signal"' ), 'net.getSigType().getName() == "signal"' )
    assertq ( eval( 'not net.isGlobal()' ), 'not net.isGlobal()' )
    assertq ( eval( 'term.getModule().getName() ==  module.getName()' ),'term.getModule().getName() == module.getName()')
    assertq ( eval( 'term.getType().getName() == "ModScalarTerm"' ), 'term.getType().getName() == "ModScalarTerm"' )
    assertq ( eval( 'int(term.getTermType()) == int(ioDir)' ), 'int(term.getTermType()) == int(ioDir)' )
    assertq ( eval( 'term.getNet().getName() == net.getName()' ), ' term.getNet().getname() == net.getName()' )


    pstr = "Term '%s' created attached to Net '%s'\n" % ( term.getName(), term.getNet().getName() )
    log( pstr )

    expect_modTerm_on_net( term, net )
    return net



def create_modInst( type, mod, master, instName ):
    # CPP lab implmenents a Templatized function that exploits the commonality of method names to
    # create Insts of different types. Changed to oaModModuleScalarInst and oaModModuleScalarInst
    # specific code for perl and python
    global ns
    inst_scname =  oa.oaScalarName( ns, instName )

    # Create an Inst using the function arguments to specify its master
    # and its owner Module. Assign to the inst variable.
    inst = 0

    if ( type == "oaModModuleScalarInst"):
        inst =  oa.oaModModuleScalarInst.create( mod, master, inst_scname)

    elif (type == "oaModScalarInst"):
        inst =  oa.oaModScalarInst.create( mod, master, inst_scname)

    if ( inst ):
        assertq(eval( 'inst.isValid()' ), 'inst->isValid()' )
        instTypeName = inst.getType().getName()
        pstr   = "Created %s '%s'\n" % (instTypeName, inst.getName() )
        log( pstr )
    
    return inst




def create_half_adder( design, cellLibName, designLibName, view ):
    global ns, mode_read
    # Create the "HalfAdder" Module in the specified design Lib and View
    # (passed as args), assigning it to the modHA variable.
    ha_scname = oa.oaScalarName( ns, "HalfAdderMod")
    modHA = oa.oaModule.create( design, ha_scname )

    # Open the "Xor" and "And" Designs from the cell Lib and View
    # (passed as args) in READ mode.
    designXor = oa.oaDesign.open( cellLibName, "Xor", view, mode_read )
    designAnd = oa.oaDesign.open( cellLibName, "And", view, mode_read )

    try:
        # Use the create_modInst helper function defined in this lab to attempt to
        # create a ModModuleInst in modHA of the top Module of the Xor Design.
        create_modInst("oaModModuleScalarInst",
                        mod_ha,
                        design_xor.getTopModule(), "Xor1q")


    except:
        print  "{As expected, cannot instantiate Modules from other Designs}"
        
    

    # Use the create_modInst helper function to create ModInsts named "Xor1"
    # and "And1" of  master Designs Xor and And to be owned/contained by modHA.
    instXor = create_modInst("oaModScalarInst", modHA, designXor, "Xor1")
    instAnd = create_modInst("oaModScalarInst", modHA, designAnd, "And1")

    log( "Creating Design 'HA'.\n" )
    add_indent()
    assertq (eval( 'modHA.isValid()' ), 'modHA.isValid()' )

    netA = add_modInstTerm( modHA, "A", oa.oacInputTermType  )
    netB = add_modInstTerm( modHA, "B", oa.oacInputTermType  )
    netC = add_modInstTerm( modHA, "C", oa.oacOutputTermType )
    netS = add_modInstTerm( modHA, "S", oa.oacOutputTermType )

    assertq ( eval( 'get_num_nets( modHA ) == 4' ), 'get_num_nets( modHA ) == 4 ' )
    assertq ( eval( 'getNumTerms( modHA ) == 4'), 'getNumTerms( modHA ) == 4' ) 

    # Call create_modInstTerm_by_name, defined above to create ModInstTerms on
    # instXor named "A", "B", "Y", connected to netA, netB, NetS, respectively.
    instTermXorA = create_modInstTerm_by_name( instXor, "A", netA )
    instTermXorB = create_modInstTerm_by_name( instXor, "B", netB )
    instTermXorY = create_modInstTerm_by_name( instXor, "Y", netS )

    # Call create_modInstTerm_by_name, defined above, to create ModInstTerms on
    # instAnd named "A", "B", "Y", connected to netA, netB, NetC, respectively.
    instTermAndA = create_modInstTerm_by_name( instAnd, "A", netA ) 
    instTermAndB = create_modInstTerm_by_name( instAnd, "B", netB )
    instTermAndY = create_modInstTerm_by_name( instAnd, "Y", netC )

    assertq( eval( 'get_num_instTerms( modHA ) == 6' ), 'get_num_instTerms( modHA ) == 6' )
    sub_indent()
    return modHA


# ---------- Create Design with Modules and Blocks

def create_design ( designLibName, cell, view ):
    global ns, mode_trunc
    # Create a Design with the names passed as arguments, maskLayout ViewType,
    # overwriting any Design of those names/type that might already exist.
    vt = oa.oaViewType.get( oa.oaReservedViewType( "maskLayout") )

    designLibName_sc = oa.oaScalarName( ns, designLibName)
    design =  oa.oaDesign.open ( designLibName_sc,
                                 cell, 
                                 view, 
                                 vt, 
                                 mode_trunc )

    assertq( eval( 'design.isValid()' ), 'design.isValid()' )
    assertq( eval( 'design.getRefCount() == 1' ), 'design.getRefCount() == 1' )
    assertq( eval( 'design.getType().getName() ==  "Design"' ), 'design.getType().getName() ==  "Design"' )

    assertq( eval( 'design.getViewType().getName() == "maskLayout"' ), 'design.getViewType().getName() == "maskLayout" ' )

    # Assign design a "block" CellType attribute value.
    design.setCellType( oa.oaCellType(oa.oacBlockCellType) )

    assertq( eval( 'int(design.getCellType()) == int(oa.oacBlockCellType)' ), 'int(design.getCellType()) == int(oa.oacBlockCellType)' )
    return design



def create_full_adder( design, cellLibName, designLibName, view ):
    global ns, mode_read
    # Open the existing design named "Or" in the CellLib in read mode.
    cellLibName_sc = oa.oaScalarName( ns, cellLibName)
    cellName_sc    = oa.oaScalarName( ns, "Or" )
    view_sc        = oa.oaScalarName( ns, view )
    designOr = oa.oaDesign.open( cellLibName_sc, "Or", view_sc, mode_read )

    # Create an embedded modFA FullAdder module owned by the Design passed as an argument.
    modFA = oa.oaModule.create( design, "FullAdderMod" )

    # Call the Create_Half_Adder utility function to create the modHA module, also owned by
    # the design, passing along the mapped names of the cell and design libraries, and View.
    modHA =  create_half_adder( design, cellLibName, designLibName, view )

    # Call the Create_ModInst utility function defined above to create in
    # the modFA parent an Inst named "Ha1" and one named "Ha2 of modHA.
    instHA1 = create_modInst( "oaModModuleScalarInst", modFA, modHA, "Ha1" )
    instHA2 = create_modInst(  "oaModModuleScalarInst", modFA, modHA, "Ha2" )

    # Call the Create_ModInst{} utility function defined above to create in
    # the modFA parent an Inst named "Or1" of designOr.
    instOr1 = create_modInst( "oaModScalarInst", modFA, designOr, "Or1" )

    #  Create nets and terminals for each I/O on this block.
    #
    netA  = add_modInstTerm( modFA, "A",  oa.oacInputTermType )
    netB  = add_modInstTerm( modFA, "B",  oa.oacInputTermType )
    netCi = add_modInstTerm( modFA, "Ci", oa.oacInputTermType )
    netCo = add_modInstTerm( modFA, "Co", oa.oacOutputTermType)
    netS  = add_modInstTerm( modFA, "S",  oa.oacOutputTermType)
      #
      # Create internal nets
      #
    netH1c = create_modNet( modFA, "H1c" )
    netH1s = create_modNet( modFA, "H1s" )
    netH2c = create_modNet( modFA, "H2c" )
    assertq( eval( 'getNumTerms( modFA ) == 5' ), 'getNumTerms( modFA ) == 5' )

    add_indent()

    # Create an iterator that collects all the ModuleInsts in modFA
    for modInst in modFA.getInsts():
        pstr ="  modInst '%s' type=%s InstHeader type=%s\n" %  ( modInst.getName(), modInst.getType().getName(), modInst.getHeader().getType().getName() )
        sys.stdout.write(pstr)

  # Create an iterator that collects all the ModuleInstHeaders in modFA
    for mmiHeader in modFA.getModuleInstHeaders():
        pstr = "     modModuleInstHeader %s\n" % ( get_mapped_name( mmiHeader ) )
        sys.stdout.write(pstr)
        # Create an iterator that collects all the Insts in the Header
        for modInst in mmiHeader.getInsts():
            pstr = "       modInst '%s'\n" % ( modInst.getName() )
            sys.stdout.write(pstr)
  # Create ModInstTerms on the HalfAdder Insts.

    create_modInstTerm_by_name( instHA1, "A", netA   )
    create_modInstTerm_by_name( instHA1, "B", netB   )
    create_modInstTerm_by_name( instHA1, "C", netH1c )
    create_modInstTerm_by_name( instHA1, "S", netH1s )

    create_modInstTerm_by_name( instHA2, "A", netH1s )
    create_modInstTerm_by_name( instHA2, "B", netCi  )
    create_modInstTerm_by_name( instHA2, "C", netH2c )
    create_modInstTerm_by_name( instHA2, "S", netS   )

    # Create ModInstTerms on the Or Insts.

    create_modInstTerm_by_name( instOr1, "A", netH1c )
    create_modInstTerm_by_name( instOr1, "B", netH2c )
    create_modInstTerm_by_name( instOr1, "Y", netCo  )

    sub_indent()
    return modFA



def create_adder_3bit( cellLibName, designLibName, view ):
    global ns, visibleToBlock
    # Create the top level 3-bit adder Design using the arguments passed in.
    #
    design3bit = create_design( designLibName, "Adder3bit", view )

    # Create a Module named "Adder3bitMod" in the newly created Design.
    mod3bit = oa.oaModule.create( design3bit, "Adder3bitMod" )

    # Set the module created as the topModule BEFORE creating more hierarchy
    # (or it won't propagate to Block domain).
    design3bit.setTopModule( mod3bit, visibleToBlock )

    # Create modFA, a FullAdder Module in the 3bit design.
     #
    modFA  = create_full_adder( design3bit, cellLibName, designLibName, view )

    # Use the Create_ModInst helper function to create inside design3bit's top Module,
    # three ModInsts of the modFA FullAdder Module just created above. Name them "Fa0",
    # "Fa1", and "Fa2". {Use the schematic drawing at the top of this file as a guide.}
    mmiAdder0 = create_modInst( "oaModModuleScalarInst", mod3bit, modFA, "Fa0" )
    mmiAdder1 = create_modInst( "oaModModuleScalarInst", mod3bit, modFA, "Fa1" )
    mmiAdder2 = create_modInst( "oaModModuleScalarInst", mod3bit, modFA, "Fa2" )

    # Create in the top module of design3bit each of the ModScalarNets declared above
    # by calling the add_modInstTerm helper function defined above, passing in as
    # the name the last two characters of the corresponding Net name.
    # For example netA0 {and the Term created for it by the helper}
    # should be named "A0", and so on.  All "S*" and "Co" Terms should
    # be Output types, with the "A*", "B*", and "Ci" Terns as Input types.
    # {Use the schematic drawing at the top of this file as a guide.}
    netA0 = add_modInstTerm( mod3bit, "A0", oa.oacInputTermType  )
    netB0 = add_modInstTerm( mod3bit, "B0", oa.oacInputTermType  )
    netS0 = add_modInstTerm( mod3bit, "S0", oa.oacOutputTermType )
    netA1 = add_modInstTerm( mod3bit, "A1", oa.oacInputTermType  )
    netB1 = add_modInstTerm( mod3bit, "B1", oa.oacInputTermType  )
    netS1 = add_modInstTerm( mod3bit, "S1", oa.oacOutputTermType )
    netA2 = add_modInstTerm( mod3bit, "A2", oa.oacInputTermType  )
    netB2 = add_modInstTerm( mod3bit, "B2", oa.oacInputTermType  )
    netS2 = add_modInstTerm( mod3bit, "S2", oa.oacOutputTermType )
    netA3 = add_modInstTerm( mod3bit, "A3", oa.oacInputTermType  )
    netB3 = add_modInstTerm( mod3bit, "B3", oa.oacInputTermType  )
    netS3 = add_modInstTerm( mod3bit, "S3", oa.oacOutputTermType )
    netCi = add_modInstTerm( mod3bit, "Ci", oa.oacInputTermType  )
    netCo = add_modInstTerm( mod3bit, "Co", oa.oacOutputTermType )

    # Create in the top module of design3bit two ModScalarNets declared above
    # by calling the Create_ModNet{} helper function defined above, passing in
    # as the names "C01" and "C02", respectively.
    netC01 = create_modNet( mod3bit, "C01" )
    netC12 = create_modNet( mod3bit, "C12" )

    # Call the Create_ModInstTerm_By_Name{} helper function defined above
    # to create ModInstTerms on each of the three FullAdder Insts for all
    # master Terms, connecting each to the corresponding Net created above.
    # {Use the schematicdrawing at the top of this file as a guide.}
    #
    itAdd0A  = create_modInstTerm_by_name( mmiAdder0, "A" , netA0  )
    itAdd0B  = create_modInstTerm_by_name( mmiAdder0, "B" , netB0  )
    itAdd0S  = create_modInstTerm_by_name( mmiAdder0, "S" , netS0  )
    itAdd0Ci = create_modInstTerm_by_name( mmiAdder0, "Ci", netCi  )
    itAdd0Co = create_modInstTerm_by_name( mmiAdder0, "Co", netC01 )
    itAdd1A  = create_modInstTerm_by_name( mmiAdder1, "A" , netA1  )
    itAdd1B  = create_modInstTerm_by_name( mmiAdder1, "B" , netB1  )
    itAdd1S  = create_modInstTerm_by_name( mmiAdder1, "S" , netS1  )
    itAdd1Ci = create_modInstTerm_by_name( mmiAdder1, "Ci", netC01 )
    itAdd1Co = create_modInstTerm_by_name( mmiAdder1, "Co", netC12 )
    itAdd2A  = create_modInstTerm_by_name( mmiAdder2, "A" , netA2  )
    itAdd2B  = create_modInstTerm_by_name( mmiAdder2, "B" , netB2  )
    itAdd2S  = create_modInstTerm_by_name( mmiAdder2, "S" , netS2  )
    itAdd2Ci = create_modInstTerm_by_name( mmiAdder2, "Ci", netC12 )
    itAdd2Co = create_modInstTerm_by_name( mmiAdder2, "Co", netCo  )

    return design3bit






# -------------------------------- main ------------------------------
oa.oaDesignInit()

ns = oa.oaNativeNS()

def main():
    mode_append  = "a"

    libNetName  = "Lib"
    libModName  = "Lib3bitAdder"
    cellName    = "Lib"
    viewName    = "netlist"
    libModDir   =  "Lib3bit"
    libDefsFile = "lib.defs"


    # Perform logical lib name to physical path name mappings for the source
    # library created in the netlist/ lab, then the symbol library to be
    # created in this lab.
    strLibDefDef = oa.oaLibDefList.getDefaultPath()

    if  ( strLibDefDef  != libDefsFile ):
        print "***Missing local %s file." % (libDefsFile)
        print "   Please create one with the one line shown below:"
        print "      INCLUDE ../netlist/lib.defs"
        sys.exit(4)
    
    oa.oaLibDefList.openLibs()

    libNetName_sc = oa.oaScalarName(ns, libNetName)
    libNet = oa.oaLib.find( libNetName_sc )

    if not libNet:
        print  "***Either lib.defs file missing or netlist lab was not completed.\n"
        sys.exit(1)
    


    # Create (or open if this lab is being rerun) the output 3bit Module
    # library.
    #
    libModName_sc = oa.oaScalarName(ns, libModName)
    libMod = oa.oaLib.find( libModName )

    if libMod:
        print "LibMod opened via openLibs of %s" % (strLibDefDef)
    else:
        if ( oa.oaLib.exists( libModDir ) ):
            #
            # Directory there but DEFINE for it missing in lib.defs.
            #
            libMod = oa.oaLib.open( libModName_sc, libModDir )
            print "Opened existing %s in directory  %s" % (libModName, libModDir)
        else:
            # No directory for LibMod.
            #
            libMod = oa.oaLib.create( libModName_sc, 
                                      libModDir, 
                                      oa.oaLibMode(oa.oacSharedLibMode), 
                                      "oaDMFileSys")
            print "Created new %s in directory %s" % (libModName, libModDir)
       
        # Add a LibDef for this new Module Lib to the lib.defs file
        #

        print "\nAppending new Lib def for %s->%s to LibDefs file = %s"  % ( libMod.getName(), libModDir, strLibDefDef)
#>>>        oa.oaLibDef.create( ldl, libModName_sc , libModDir)
#>>>        ldl.save()
#>>>
#>>> Problems with oaPerl and oaPython getting an append of the  libMod info to the ./lib.defs file.
#>>> Experiments show that the save() truncated any blank lines after the INCLUDE ../netlist/lib.defs,
#>>> but never adds the lines for Lib3bit. Workaround to use oaFile now to fix the lib.defs and go on.
#>>>    oa.oaFile("./lib.defs") terminated with "TypeError: in method 'oaFile_write', argument 2 of type 'void *'".
#>>> See bug report 1335: http://www.si2.org/openeda.si2.org/tracker/?func=detail&atid=314&aid=1335&group_id=78
#>>> Finish lab with plain python append to lib.defs which is needed for oadump post process step.
     
        libdefsfile = open ('./lib.defs', 'a')
        str = "\nDEFINE Lib3bitAdder Lib3bit\nASSIGN Lib3bitAdder writePath Lib3bit\nASSIGN Lib3bitAdder libMode shared\n"
        libdefsfile.write(str)
        libdefsfile.close()




    # Pretend to "import" Verilog descriptions of design modules
    #
    #    module Adder3bit(Co, S0,S1,S2,S3, A0,A1,A2,A3, B0,B1,B2,B3, Ci);
    #      input A0,A1,A2,A3, B0,B1,B2,B3, Ci;
    #      output Co, S0,S1,S2,S3;
    #      FA Add0 (Co,S,A,B,Ci);
    #      FA Add1 (Co,S,A,B,Ci);
    #      FA Add2 (Co,S,A,B,Ci);
    #      FA Add3 (Co,S,A,B,Ci);
    #    endmodule
    #    module FullAdder(Co,S,A,B,Ci);
    #      input A,B,Ci;
    #      output Co,S;
    #      HA Ha1 (C,S,A,B);
    #      HA Ha2 (C,S,A,B);
    #      Or Or1 (Y,A,B);
    #    endmodule
    #    module HalfAdder(C,S,A,B);
    #      input A,B;
    #      output C,S;
    #      And And1 (Y,A,B);
    #      Xor Xor1 (Y,A,B);
    #    endmodule

    # Use the Create_Adder_3bit function defined in this lab to
    # create design3bit using the mapped names of the:
    #    - cell library (from the netlist lab) containing the leaf OR, AND, XOR gates
    #    - design library to hold the 3-bit Adder begin created in this lab
    #    - View name to be used for all the design containers created.
    design3bit = create_adder_3bit( libNetName, libModName, viewName)

    # Save the design at this initial stage BEFORE any partitioning or
    # clustering is done to it.
    design3bit.save()

    dump_open_designs(True, True, False)

    new_section( "Detach FullAdderMod" )

    # Locate the full-adder module by name in the Design
    modFA  = oa.oaModule.find( design3bit, "FullAdderMod" )
    assertq( eval( 'modFA.isValid()' ), 'modFA.isValid()' )

    # Detach the full-adder Module hierarchy from Design 3bitAdder creating a new
    # Design in the same Lib, with the same View name, but a Cell name of "FAdetach".
    desFAdet = modFA.detach( libModName, "FAdetach", viewName )

    dump_open_designs(True, True, False)

    new_section( "Embed Or" )

    # Embed the top Module of the Or Design into the new FADetach Design.
    orModuleToEmbed = oa.oaDesign.open( libNetName, "Or", viewName, mode_read )
    oa.oaModule.embed( desFAdet, orModuleToEmbed )

    dump_open_designs(True, True, False)

    design3bit.close() 
    desFAdet.close()

    # NOTE!!!! Si2 C++ module lab (and this version of it) DOES NOT SAVE the des_fa_detatch design.
    # Note that the Lib3bit/FAdetach/netlist: will have a 0 length layout.oa
    # Attempts to oadump or emhlister dump this lib-cell-view will report
    # ***ERROR: Can't read Design Lib3bitAdder/FAdetach/netlist: \
    # File '..path../Lib3bit/FAdetach/netlist/layout.oa' does not exist, or is not a valid OpenAccess database.


    print "\n...........Normal Termination........\n"




if (__name__ == "__main__"):
    sys.exit(main())


